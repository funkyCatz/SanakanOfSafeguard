# Sanakan of safeguard
#### Simple twitch.tv bot on .net core

It was made just for fun and test new technologies.

Name is reference to the Tsutomu Nihei's "Blame" manga character.
 
Project uses :
 - MediatR for CQRS implementation and for small service bus between TwitchService and ChatService.
 - AutoMapper for mapping between Domain entities and Database models
 - NUnit and FluentAssertions for unit testing  
 - RavenDB and MongoDB for storing user event logs and filtration messages (you should choose only one DB)
 
 There is also branch where I tried (and actually did) to split this project into microservices, but I've dropped that idea.
 
 ### How does it work
 
 The core of this project is a ChatService and TwitchChatService.
 ChatService connects to the twitch IRC server via TCP (right now it supports only TCP).
 ChatService reads messages from the TCP stream and sends them to the TwitchChatService using MediatR NotificationHandler.
 
 TwitchChatService parses raw message string and creates instance of IrcMessage entity.
 It is also able to filtrate messages (see `IFilter` interface) and execute commands (see `ITwitchCommand` interface).
 

  