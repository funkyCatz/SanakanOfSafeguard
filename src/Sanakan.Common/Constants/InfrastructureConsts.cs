﻿namespace Sanakan.Common.Constants
{
    public static class InfrastructureConsts
    {
        public static class SerilogConfig
        {
            public static string ConsoleOutputTemplate = "[{Timestamp:HH:mm:ss} {Level:u3}] {Message:lj} {SourceContext}>{NewLine}{Exception}";
            public static string FileOutputTemplate = "{Timestamp:o} [{Level:u3}] ({Application}/{MachineName}/{ThreadId}) {Message}{NewLine}{Exception}";
        }
    }
}