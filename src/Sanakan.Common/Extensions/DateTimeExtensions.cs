﻿using System;

namespace Sanakan.Common.Extensions
{
    public static class DateTimeExtensions
    {
        public static DateTime ParseUnixDateTime(string unixDateTime)
        {
            double.TryParse(unixDateTime, out var parsedUnixTime);
            DateTime dtDateTime = new DateTime(1970,1,1,0,0,0,0,DateTimeKind.Utc);
            return dtDateTime.AddMilliseconds(parsedUnixTime).ToUniversalTime();
        }
    }
}