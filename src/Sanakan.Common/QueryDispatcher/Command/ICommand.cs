﻿using Sanakan.Common.QueryDispatcher.Query;

namespace Sanakan.Common.QueryDispatcher.Command
{
    public interface ICommand : IQuery<ICommandResult> { }

    public interface ICommand<out TCommandResult> : IQuery<TCommandResult>
        where TCommandResult : ICommandResult
    {

    }
}
