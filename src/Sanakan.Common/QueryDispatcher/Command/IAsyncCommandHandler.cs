﻿namespace Sanakan.Common.QueryDispatcher.Command
{
    public interface IAsyncCommandHandler<in TCommand>
        : IAsyncQueryHandler<TCommand, ICommandResult>
    {
    }
}
