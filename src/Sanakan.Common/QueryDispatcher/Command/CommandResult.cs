﻿using System.Collections.Generic;

namespace Sanakan.Common.QueryDispatcher.Command
{
    public class CommandResult : ICommandResult
    {
        public bool Successful { get; }

        public CommandResult(bool successful = true)
        {
            Successful = successful;
        }

        public IEnumerable<IValidationItem> Errors { get; }
        public IEnumerable<IValidationItem> Warnings { get; }
        public IEnumerable<string> SuccessSummary { get; }
    }
}
