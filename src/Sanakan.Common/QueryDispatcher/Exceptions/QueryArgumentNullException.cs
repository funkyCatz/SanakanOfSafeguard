﻿using System;

namespace Sanakan.Common.QueryDispatcher.Exceptions
{
    public class QueryArgumentNullException : ArgumentException
    {
    }
}
