using System.Threading.Tasks;
using Sanakan.Common.QueryDispatcher.Query;

namespace Sanakan.Common.QueryDispatcher
{
    public interface IQueryDispatcher
    {
        TResult FindAndExecute<TResult>(IQuery<TResult> query);
        TResult FindAndExecuteWithKey<TResult>(IQuery<TResult> query, object key);
        Task<TResult> FindAndExecuteAsync<TResult>(IQuery<TResult> query);
    }
}