﻿using System;
using System.Runtime.CompilerServices;

namespace Sanakan.Common.Models.TwitchModels
{
    public readonly struct GiftedSubscription
    {
        public string Badges { get; }
        public string Color { get; }
        public string DisplayName { get; }
        public string Emotes { get; }
        public string Id { get; }
        public bool IsModerator { get; }
        public bool IsSubscriber { get; }
        public bool IsTurbo { get; }
        public string Login { get; }
        public string MsgId { get; }
        public string MsgParamMonths { get; }
        public string MsgParamRecipientDisplayName { get; }
        public string MsgParamRecipientId { get; }
        public string MsgParamRecipientUserName { get; }
        public string MsgParamSubPlanName { get; }
        public SubscriptionPlanEnum MsgParamSubPlan { get; }
        public string RoomId { get; }
        public string SystemMsg { get; }
        public string SystemMsgParsed { get; }
        public string TmiSentTs { get; }
        public UserTypeEnum UserType { get; }

        public GiftedSubscription(IrcMessage ircMessage) : this()
        {
            foreach (var tag in ircMessage.Tags.Keys)
            {
                var tagValue = ircMessage.Tags[tag];

                switch (tag)
                {
                    case Tags.Badges:
                        Badges = tagValue;
                        break;
                    case Tags.Color:
                        Color = tagValue;
                        break;
                    case Tags.DisplayName:
                        DisplayName = tagValue;
                        break;
                    case Tags.Emotes:
                        Emotes = tagValue;
                        break;
                    case Tags.Id:
                        Id = tagValue;
                        break;
                    case Tags.Login:
                        Login = tagValue;
                        break;
                    case Tags.Mod:
                        IsModerator = ConvertToBool(tagValue);
                        break;
                    case Tags.MsgId:
                        MsgId = tagValue;
                        break;
                    case Tags.MsgParamMonths:
                        MsgParamMonths = tagValue;
                        break;
                    case Tags.MsgParamRecipientDisplayname:
                        MsgParamRecipientDisplayName = tagValue;
                        break;
                    case Tags.MsgParamRecipientId:
                        MsgParamRecipientId = tagValue;
                        break;
                    case Tags.MsgParamRecipientUsername:
                        MsgParamRecipientUserName = tagValue;
                        break;
                    case Tags.MsgParamSubPlanName:
                        MsgParamSubPlanName = tagValue;
                        break;
                    case Tags.MsgParamSubPlan:
                        MsgParamSubPlan = tagValue switch
                        {
                            "prime" => SubscriptionPlanEnum.Prime,
                            "1000" => SubscriptionPlanEnum.Tier1,
                            "2000" => SubscriptionPlanEnum.Tier2,
                            "3000" => SubscriptionPlanEnum.Tier3,
                            _ => throw new ArgumentOutOfRangeException(nameof(tagValue.ToLower))
                        };
                        break;
                    case Tags.RoomId:
                        RoomId = tagValue;
                        break;
                    case Tags.Subscriber:
                        IsSubscriber = ConvertToBool(tagValue);
                        break;
                    case Tags.SystemMsg:
                        SystemMsg = tagValue;
                        SystemMsgParsed = tagValue.Replace("\\s", " ").Replace("\\n", "");
                        break;
                    case Tags.TmiSentTs:
                        TmiSentTs = tagValue;
                        break;
                    case Tags.Turbo:
                        IsTurbo = ConvertToBool(tagValue);
                        break;
                    case Tags.UserType:
                        UserType = tagValue switch
                        {
                            "mod" => UserTypeEnum.Moderator,
                            "global_mod" => UserTypeEnum.GlobalModerator,
                            "admin" => UserTypeEnum.Admin,
                            "staff" => UserTypeEnum.Staff,
                            _ => UserTypeEnum.Viewer
                        };
                        break;
                }
            }
        }
        
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private bool ConvertToBool(string data) => data == "1";
    }
}
