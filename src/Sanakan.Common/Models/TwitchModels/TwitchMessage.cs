﻿using System.Collections.Generic;
using System.Drawing;

namespace Sanakan.Common.Models.TwitchModels
{
    public abstract class TwitchMessage
    {
        public IDictionary<string, string> Badges { get; protected set; }
        public string BotUsername { get; protected set; }
        public Color Color { get; protected set; }
        public string ColorHex { get; protected set; }
        public string DisplayName { get; protected set; }
        public EmoteSet EmoteSet { get; protected set; }
        public bool IsTurbo { get; protected set; }
        public string UserId { get; protected set; }
        public string UserName { get; protected set; }
        public UserTypeEnum UserType { get; protected set; }
    }
}
