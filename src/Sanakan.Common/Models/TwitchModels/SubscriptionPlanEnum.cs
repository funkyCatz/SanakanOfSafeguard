﻿namespace Sanakan.Common.Models.TwitchModels
{
    public enum SubscriptionPlanEnum
    {
        NotSet,
        Prime,
        Tier1,
        Tier2,
        Tier3
    }
}
