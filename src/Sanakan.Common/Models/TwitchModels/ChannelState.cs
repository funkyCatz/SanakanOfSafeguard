﻿using System;
using System.Runtime.CompilerServices;

namespace Sanakan.Common.Models.TwitchModels
{
    public readonly struct ChannelState
    {
        public string BroadcasterLanguage { get; }
        public string Channel { get; }
        public bool? EmoteOnly { get; }
        public TimeSpan FollowersOnly { get; }
        public bool Mercury { get; }
        public bool? R9K { get; }
        public bool? Rituals { get; }
        public string RoomId { get; }
        public int? SlowMode { get; }
        public bool? SubOnly { get; }

        public ChannelState(IrcMessage ircMessage) : this()
        {
            //@broadcaster-lang=;emote-only=0;r9k=0;slow=0;subs-only=1 :tmi.twitch.tv ROOMSTATE #burkeblack
            foreach (var tag in ircMessage.Tags.Keys)
            {
                var tagValue = ircMessage.Tags[tag];

                switch (tag)
                {
                    case Tags.BroadcasterLang:
                        BroadcasterLanguage = tagValue;
                        break;
                    case Tags.EmoteOnly:
                        EmoteOnly = ConvertToBool(tagValue);
                        break;
                    case Tags.R9K:
                        R9K = ConvertToBool(tagValue);
                        break;
                    case Tags.Rituals:
                        Rituals = ConvertToBool(tagValue);
                        break;
                    case Tags.Slow:
                        var success = int.TryParse(tagValue, out var slowDuration);
                        SlowMode = success ? slowDuration : (int?)null;
                        break;
                    case Tags.SubsOnly:
                        SubOnly = ConvertToBool(tagValue);
                        break;
                    case Tags.FollowersOnly:
                        var minutes = int.Parse(tagValue);
                        FollowersOnly = TimeSpan.FromMinutes(minutes == -1 ? 0 : minutes);
                        break;
                    case Tags.RoomId:
                        RoomId = tagValue;
                        break;
                    case Tags.Mercury:
                        Mercury = ConvertToBool(tagValue);
                        break;
                    default:
                        Console.WriteLine("[TwitchLib][ChannelState] Unaccounted for: " + tag);
                        break;
                }
            }
            Channel = ircMessage.Channel;
        }
        
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private bool ConvertToBool(string data) =>  data == "1";
    }
}
