﻿namespace Sanakan.Common.Models.TwitchModels
{
    public enum BadgeColorEnum
    {
        Red = 10000,
        Blue = 5000,
        Green = 1000,
        Purple = 100,
        Gray = 1
    }
}
