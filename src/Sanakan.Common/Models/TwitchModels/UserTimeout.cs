﻿using System;

namespace Sanakan.Common.Models.TwitchModels
{
    public readonly struct UserTimeout
    {
        public string Channel { get; }
        public int TimeoutDuration { get; }
        public string TimeoutReason { get; }
        public string Username { get; }

        public UserTimeout(IrcMessage ircMessage)
        {
            Channel = ircMessage.Channel;
            Username = ircMessage.Message;
            TimeoutDuration = 0;
            TimeoutReason = String.Empty;

            foreach (var tag in ircMessage.Tags.Keys)
            {
                var tagValue = ircMessage.Tags[tag];

                switch (tag)
                {
                    case Tags.BanDuration:
                        TimeoutDuration = int.Parse(tagValue);
                        break;
                    case Tags.BanReason:
                        TimeoutReason = tagValue;
                        break;
                }
            }
        }
    }
}
