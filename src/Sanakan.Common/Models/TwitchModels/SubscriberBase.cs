﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Runtime.CompilerServices;

namespace Sanakan.Common.Models.TwitchModels
{
    public class SubscriberBase
    {
        public IDictionary<string, string> Badges { get; }
        public string ColorHex { get; }
        public Color Color { get; }
        public string DisplayName { get; }
        public string EmoteSet { get; }
        public string Id { get; }
        public bool IsModerator { get; }
        public bool IsPartner { get; }
        public bool IsSubscriber { get; }
        public bool IsTurbo { get; }
        public string Login { get; }
        public string RawIrc { get; }
        public string ResubMessage { get; }
        public string RoomId { get; }
        public SubscriptionPlanEnum SubscriptionPlan { get; } = SubscriptionPlanEnum.NotSet;
        public string SubscriptionPlanName { get; }
        public string SystemMessage { get; }
        public string SystemMessageParsed { get; }
        public string TmiSentTs { get; }
        public string UserId { get; }
        public UserTypeEnum UserType { get; }

        // @badges=subscriber/1,turbo/1;color=#2B119C;display-name=JustFunkIt;emotes=;id=9dasn-asdibas-asdba-as8as;login=justfunkit;mod=0;msg-id=resub;msg-param-months=2;room-id=44338537;subscriber=1;system-msg=JustFunkIt\ssubscribed\sfor\s2\smonths\sin\sa\srow!;turbo=1;user-id=26526370;user-type= :tmi.twitch.tv USERNOTICE #burkeblack :AVAST YEE SCURVY DOG

        protected readonly int months;

        /// <summary>Subscriber object constructor.</summary>
        protected SubscriberBase(IrcMessage ircMessage)
        {
            RawIrc = ircMessage.ToString();
            ResubMessage = ircMessage.Message;

            foreach (var tag in ircMessage.Tags.Keys)
            {
                var tagValue = ircMessage.Tags[tag];
                switch (tag)
                {
                    case Tags.Badges:
                        Badges = new Dictionary<string, string>();
                        foreach (var badgeValue in tagValue.Split(','))
                            if (badgeValue.Contains('/'))
                                Badges.Add(new KeyValuePair<string, string>(badgeValue.Split('/')[0], badgeValue.Split('/')[1]));
                        // iterate through badges for special circumstances
                        foreach (var badge in Badges)
                        {
                            if (badge.Key == "partner")
                                IsPartner = true;
                        }
                        break;
                    case Tags.Color:
                        ColorHex = tagValue;
                        if (!string.IsNullOrEmpty(ColorHex))
                            Color = ColorTranslator.FromHtml(ColorHex);
                        break;
                    case Tags.DisplayName:
                        DisplayName = tagValue;
                        break;
                    case Tags.Emotes:
                        EmoteSet = tagValue;
                        break;
                    case Tags.Id:
                        Id = tagValue;
                        break;
                    case Tags.Login:
                        Login = tagValue;
                        break;
                    case Tags.Mod:
                        IsModerator = ConvertToBool(tagValue);
                        break;
                    case Tags.MsgParamMonths:
                        months = int.Parse(tagValue);
                        break;
                    case Tags.MsgParamSubPlan:
                        switch (tagValue.ToLower())
                        {
                            case "prime":
                                SubscriptionPlan = SubscriptionPlanEnum.Prime;
                                break;
                            case "1000":
                                SubscriptionPlan = SubscriptionPlanEnum.Tier1;
                                break;
                            case "2000":
                                SubscriptionPlan = SubscriptionPlanEnum.Tier2;
                                break;
                            case "3000":
                                SubscriptionPlan = SubscriptionPlanEnum.Tier3;
                                break;
                            default:
                                throw new ArgumentOutOfRangeException(nameof(tagValue.ToLower));
                        }
                        break;
                    case Tags.MsgParamSubPlanName:
                        SubscriptionPlanName = tagValue.Replace("\\s", " ");
                        break;
                    case Tags.RoomId:
                        RoomId = tagValue;
                        break;
                    case Tags.Subscriber:
                        IsSubscriber = ConvertToBool(tagValue);
                        break;
                    case Tags.SystemMsg:
                        SystemMessage = tagValue;
                        SystemMessageParsed = tagValue.Replace("\\s", " ");
                        break;
                    case Tags.TmiSentTs:
                        TmiSentTs = tagValue;
                        break;
                    case Tags.Turbo:
                        IsTurbo = ConvertToBool(tagValue);
                        break;
                    case Tags.UserId:
                        UserId = tagValue;
                        break;
                    case Tags.UserType:
                        switch (tagValue)
                        {
                            case "mod":
                                UserType = UserTypeEnum.Moderator;
                                break;
                            case "global_mod":
                                UserType = UserTypeEnum.GlobalModerator;
                                break;
                            case "admin":
                                UserType = UserTypeEnum.Admin;
                                break;
                            case "staff":
                                UserType = UserTypeEnum.Staff;
                                break;
                            default:
                                UserType = UserTypeEnum.Viewer;
                                break;
                        }
                        break;
                }
            }
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private bool ConvertToBool(string data) =>  data == "1";

        public override string ToString()
        {
            return $"Badges: {Badges.Count}, color hex: {ColorHex}, display name: {DisplayName}, emote set: {EmoteSet}, login: {Login}, system message: {SystemMessage}, " +
                $"resub message: {ResubMessage}, months: {months}, room id: {RoomId}, user id: {UserId}, mod: {IsModerator}, turbo: {IsTurbo}, sub: {IsSubscriber}, user type: {UserType}, raw irc: {RawIrc}";
        }
    }
}
