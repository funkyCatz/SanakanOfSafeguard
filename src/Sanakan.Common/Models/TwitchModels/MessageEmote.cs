﻿using System.Collections.ObjectModel;
using System.Text.RegularExpressions;

namespace Sanakan.Common.Models.TwitchModels
{
    public class MessageEmote
    {
        /// <summary>
        ///     Delegate allowing Emotes to handle their replacement text on a case-by-case basis.
        /// </summary>
        /// <returns>The string for the calling emote to be replaced with.</returns>
        public delegate string ReplaceEmoteDelegate(MessageEmote caller);

        /// <summary>
        ///     Collection of Composite Format Strings which will substitute an
        ///     emote ID to get a URL for an image from the Twitch CDN
        /// </summary>
        /// <remarks>
        ///     These are sorted such that the <see cref="EmoteSize"/> enum can be used as an index,
        ///     eg TwitchEmoteUrls[<see cref="EmoteSize.Small"/>]
        /// </remarks>
        public static readonly ReadOnlyCollection<string> TwitchEmoteUrls = new ReadOnlyCollection<string>(
            new[]
            {
                "https://static-cdn.jtvnw.net/emoticons/v1/{0}/1.0",
                "https://static-cdn.jtvnw.net/emoticons/v1/{0}/2.0",
                "https://static-cdn.jtvnw.net/emoticons/v1/{0}/3.0"
            }
        );

        #region Third-Party Emote URLs
        //As this is a Twitch Library these could understandably be removed, but they are rather handy

        /// <summary>
        ///     Collection of Composite Format Strings which will substitute an
        ///     emote ID to get a URL for an image from the FFZ CDN
        /// </summary>
        /// <remarks>
        ///     These are sorted such that the <see cref="EmoteSize"/> enum can be used as an index,
        ///     eg FrankerFaceZEmoteUrls[<see cref="EmoteSize.Small"/>]
        ///     WARNING: FrankerFaceZ does not require users to submit all sizes,
        ///     so using something other than Small images may result in broken links!
        /// </remarks>
        public static readonly ReadOnlyCollection<string> FrankerFaceZEmoteUrls = new ReadOnlyCollection<string>(
            new[]
            {
                "//cdn.frankerfacez.com/emoticon/{0}/1",
                "//cdn.frankerfacez.com/emoticon/{0}/2",
                "//cdn.frankerfacez.com/emoticon/{0}/4"
            }
        );
        /// <summary>
        ///     Collection of Composite Format Strings which will substitute
        ///     an emote ID to get a URL for an image from the BTTV CDN
        ///     </summary>
        /// <remarks>
        ///     These are sorted such that the <see cref="EmoteSize"/> enum can be used as an index,
        ///     eg BetterTwitchTvEmoteUrls[<see cref="EmoteSize.Small"/>]
        /// </remarks>
        public static readonly ReadOnlyCollection<string> BetterTwitchTvEmoteUrls = new ReadOnlyCollection<string>(
            new[]
            {
                "//cdn.betterttv.net/emote/{0}/1x",
                "//cdn.betterttv.net/emote/{0}/2x",
                "//cdn.betterttv.net/emote/{0}/3x"
            }
        );
        #endregion Third-Party Emote URLs

        /// <summary>
        ///     A delegate which attempts to match the calling <see cref="MessageEmote"/> with its
        ///     <see cref="EmoteSource"/> and pulls the <see cref="EmoteSize.Small">small</see> version
        ///     of the URL.
        /// </summary>
        /// <param name="caller"></param>
        /// <returns></returns>
        private static string SourceMatchingReplacementText(MessageEmote caller)
        {
            var sizeIndex = (int)caller.Size;
            switch (caller.Source)
            {
                case EmoteSource.BetterTwitchTv:
                    return string.Format(BetterTwitchTvEmoteUrls[sizeIndex], caller.Id);
                case EmoteSource.FrankerFaceZ:
                    return string.Format(FrankerFaceZEmoteUrls[sizeIndex], caller.Id);
                case EmoteSource.Twitch:
                    return string.Format(TwitchEmoteUrls[sizeIndex], caller.Id);
            }
            return caller.Text;
        }

        /// <summary> Enum supplying the supported sites which provide Emote images.</summary>
        public enum EmoteSource
        {
            /// <summary>Emotes hosted by Twitch.tv natively</summary>
            Twitch,
            /// <summary>Emotes hosted by FrankerFaceZ.com</summary>
            FrankerFaceZ,
            /// <summary>Emotes hosted by BetterTTV.net</summary>
            BetterTwitchTv
        }

        /// <summary> Enum denoting the emote sizes</summary>
        public enum EmoteSize
        {
            /// <summary>
            ///     Best support
            ///     Small-sized emotes are the standard size used in the Twitch web chat.
            /// </summary>
            Small = 0,
            /// <summary>
            ///     Medium-sized emotes are not supported by all browsers, and
            ///     FrankerFaceZ does not require emotes to be submitted in this size
            /// </summary>
            Medium = 1,
            /// <summary>
            ///     Large-sized emotes are not supported by all browsers, and
            ///     FrankerFaceZ does not require emotes to be submitted in this size
            ///     </summary>
            Large = 2
        }

        private readonly string _id, _text, _escapedText;
        private readonly EmoteSource _source;
        private readonly EmoteSize _size;

        /// <summary>
        ///     Emote ID as used by the emote source. Will be provided as {0}
        ///     to be substituted into the indicated URL if needed.
        /// </summary>
        public string Id => _id;

        /// <summary>
        ///     Emote text which appears in a message and is meant to be replaced by the emote image.
        /// </summary>
        public string Text => _text;

        /// <summary>
        ///     The specified <see cref="EmoteSource"/> for this emote.
        /// </summary>
        public EmoteSource Source => _source;

        /// <summary>
        ///     The specified <see cref="EmoteSize"/> for this emote.
        /// </summary>
        public EmoteSize Size => _size;

        /// <summary>
        ///    The string to substitute emote text for.
        /// </summary>
        public string ReplacementString => ReplacementDelegate(this);

        /// <summary>
        ///     The desired <see cref="ReplaceEmoteDelegate"/> to use for replacing text in a given emote.
        ///     Default: <see cref="SourceMatchingReplacementText(MessageEmote)"/>
        /// </summary>
        public static ReplaceEmoteDelegate ReplacementDelegate { get; set; } = SourceMatchingReplacementText;

        /// <summary>
        ///     The emote text <see cref="Regex.Escape(string)">regex-escaped</see>
        ///     so that it can be embedded into a regex pattern.
        /// </summary>
        public string EscapedText => _escapedText;

        /// <summary>
        ///     Constructor for a new MessageEmote instance.
        /// </summary>
        /// <param name="id">
        ///     The unique identifier which the emote provider uses to generate CDN URLs.
        /// </param>
        /// <param name="text">
        ///     The string which users type to create this emote in chat.
        /// </param>
        /// <param name="source">
        ///     An <see cref="EmoteSource"/> where an image can be found for this emote.
        ///     Default: <see cref="EmoteSource.Twitch"/>
        /// </param>
        /// <param name="size">
        ///     An <see cref="EmoteSize"/> to pull for this image.
        ///     Default: <see cref="EmoteSize.Small"/>
        /// </param>
        /// <param name="replacementDelegate">
        ///     A string (optionally Composite Format with "{0}" representing
        ///     <paramref name="id"/>) which will be used instead of any of the emote URLs.
        ///     Default: null
        /// </param>
        public MessageEmote(string id, string text,
            EmoteSource source = EmoteSource.Twitch,
            EmoteSize size = EmoteSize.Small,
            ReplaceEmoteDelegate replacementDelegate = null)
        {
            _id = id;
            _text = text;
            _escapedText = Regex.Escape(text);
            _source = source;
            _size = size;
            if (replacementDelegate != null)
            {
                ReplacementDelegate = replacementDelegate;
            }
        }
    }
}
