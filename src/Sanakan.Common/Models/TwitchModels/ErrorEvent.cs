﻿namespace Sanakan.Common.Models.TwitchModels
{
    public class ErrorEvent
    {
        public string Message { get; set; }
    }
}
