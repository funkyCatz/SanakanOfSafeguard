﻿using System.Collections.Generic;

namespace Sanakan.Common.Models.TwitchModels
{
    public readonly struct SentMessage
    {
        public IDictionary<string, string> Badges { get; }
        public string Channel { get; }
        public string ColorHex { get; }
        public string DisplayName { get; }
        public string EmoteSet { get; }
        public bool IsModerator { get; }
        public bool IsSubscriber { get; }
        public string Message { get; }
        public UserTypeEnum UserType { get; }

        public SentMessage(UserState state, string message)
        {
            Badges = state.Badges;
            Channel = state.Channel;
            ColorHex = state.ColorHex;
            DisplayName = state.DisplayName;
            EmoteSet = state.EmoteSet;
            IsModerator = state.IsModerator;
            IsSubscriber = state.IsSubscriber;
            UserType = state.UserType;
            Message = message;
        }
    }
}
