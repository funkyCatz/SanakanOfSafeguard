﻿using System;
using System.Linq;

namespace Sanakan.Common.Models.TwitchModels
{
    public readonly struct BeingHostedNotification
    {
        public string BotUsername { get; }
        public string Channel { get; }
        public string HostedByChannel { get; }
        public bool IsAutoHosted { get; }
        public int Viewers { get; }


        public BeingHostedNotification(string botUsername, IrcMessage ircMessage) : this()
        {
            Channel = ircMessage.Channel;
            BotUsername = botUsername;
            HostedByChannel = ircMessage.Message.Split(' ').First();

            if (ircMessage.Message.Contains("up to "))
            {
                var split = ircMessage.Message.Split(new string[] { "up to " }, StringSplitOptions.None);
                if (split[1].Contains(" ") && int.TryParse(split[1].Split(' ')[0], out int n))
                {
                    Viewers = n;
                }
            }

            if (ircMessage.Message.Contains("auto hosting"))
            {
                IsAutoHosted = true;
            }
        }
    }
}
