﻿namespace Sanakan.Common.Models.TwitchModels
{
    public readonly struct ClearMessage
    {
        public string Channel { get; }
        public string Message { get; }
        public string Username { get; }
        public string MessageId { get; }

        public ClearMessage(string channel, string message, string username, string messageId)
        {
            Channel = channel;
            Message = message;
            Username = username;
            MessageId = messageId;
        }

        public ClearMessage(IrcMessage message)
        {
            Channel = message.Channel;
            Message = message.Message;
            Username = message.User;
            MessageId = message.Tags[Tags.MessageId];
        }

        public override string ToString () => $"@login={Username.ToLower()};target-msg-id={MessageId} :tmi.twitch.tv CLEARMSG #{Channel.ToLower()} :{Message}";
    }
}
