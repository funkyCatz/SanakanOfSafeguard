﻿namespace Sanakan.Common.Models.TwitchModels
{
    public readonly struct Emote
    {
        public int Id { get; }
        public string Name { get; }
        public int StartIndex { get; }
        public int EndIndex { get; }
        public string ImageUrl { get; }

        public Emote(int emoteId, string name, int emoteStartIndex, int emoteEndIndex) =>
            (Id, Name, StartIndex, EndIndex, ImageUrl) = (emoteId, name, emoteStartIndex, emoteEndIndex, $"https://static-cdn.jtvnw.net/emoticons/v1/{emoteId}/1.0");
    }
}
