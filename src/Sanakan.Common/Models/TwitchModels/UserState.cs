﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Text;

namespace Sanakan.Common.Models.TwitchModels
{
    public readonly struct UserState
    {
        public IDictionary<string, string> Badges { get; } 
        public string Channel { get; }
        public string ColorHex { get; }
        public string DisplayName { get; }
        public string EmoteSet { get; }
        public string BadgeInfo { get; }
        public bool IsModerator { get; }
        public bool IsSubscriber { get; }
        public UserTypeEnum UserType { get; }

        public UserState(IrcMessage ircMessage) : this()
        {
            Channel = ircMessage.Channel;
            Badges = new Dictionary<string, string>();

            foreach (var tag in ircMessage.Tags.Keys)
            {
                var tagValue = ircMessage.Tags[tag];
                switch (tag)
                {
                    case Tags.Badges:
                        if (tagValue.Contains('/'))
                        {
                            if (!tagValue.Contains(","))
                                Badges.Add(new KeyValuePair<string, string>(tagValue.Split('/')[0], tagValue.Split('/')[1]));
                            else
                                foreach (var badge in tagValue.Split(','))
                                    Badges.Add(new KeyValuePair<string, string>(badge.Split('/')[0], badge.Split('/')[1]));
                        }
                        break;
                    case Tags.Color:
                        ColorHex = tagValue;
                        break;
                    case Tags.DisplayName:
                        DisplayName = tagValue;
                        break;
                    case Tags.EmotesSets:
                        EmoteSet = tagValue;
                        break;
                    case Tags.Mod:
                        IsModerator = ConvertToBool(tagValue);
                        break;
                    case Tags.Subscriber:
                        IsSubscriber = ConvertToBool(tagValue);
                        break;
                    case Tags.BadgesInfo:
                        BadgeInfo = tagValue;
                        break;
                    case Tags.UserType:
                        UserType = tagValue switch
                        {
                            "mod" => UserTypeEnum.Moderator,
                            "global_mod" => UserTypeEnum.GlobalModerator,
                            "admin" => UserTypeEnum.Admin,
                            "staff" => UserTypeEnum.Staff,
                            _ => UserTypeEnum.Viewer
                        };
                        break;
                    default:
                        Console.WriteLine($"Unaccounted for [UserState]: {tag}");
                        break;
                }
            }

            if (string.Equals(ircMessage.User, Channel, StringComparison.InvariantCultureIgnoreCase))
                UserType = UserTypeEnum.Broadcaster;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private bool ConvertToBool(string tag) => tag == "1";
    }
}
