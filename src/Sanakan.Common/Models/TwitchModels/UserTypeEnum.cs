﻿namespace Sanakan.Common.Models.TwitchModels
{
    public enum UserTypeEnum
    {
        Viewer,
        Moderator,
        GlobalModerator,
        Broadcaster,
        Admin,
        Staff
    }
}
