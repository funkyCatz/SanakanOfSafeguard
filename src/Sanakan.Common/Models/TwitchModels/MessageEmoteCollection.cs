﻿using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace Sanakan.Common.Models.TwitchModels
{
    public class MessageEmoteCollection
    {
        private readonly SortedList<string, MessageEmote> _emoteList;
        private const string BasePattern = @"(\b{0}\b)";
        /// <summary> Do not access directly! Backing field for <see cref="CurrentPattern"/> </summary>
        private string _currentPattern;
        private Regex _regex;
        private readonly EmoteFilterDelegate _preferredFilter;

        /// <summary>
        ///     Property so that we can be confident <see cref="PatternChanged"/>
        ///     always reflects changes to <see cref="CurrentPattern"/>.
        /// </summary>
        private string CurrentPattern
        {
            get => _currentPattern;
            set
            {
                if (_currentPattern != null && _currentPattern.Equals(value)) return;
                _currentPattern = value;
                PatternChanged = true;
            }
        }

        private Regex CurrentRegex
        {
            get
            {
                if (PatternChanged)
                {
                    if (CurrentPattern != null)
                    {
                        _regex = new Regex(string.Format(CurrentPattern, ""));
                        PatternChanged = false;
                    }
                    else
                    {
                        _regex = null;
                    }
                }
                return _regex;
            }
        }

        private bool PatternChanged { get; set; }

        private EmoteFilterDelegate CurrentEmoteFilter { get; set; } = AllInclusiveEmoteFilter;

        /// <summary>
        ///     Default, empty constructor initializes the list and sets the preferred
        ///     <see cref="EmoteFilterDelegate"/> to <see cref="AllInclusiveEmoteFilter(MessageEmote)"/>
        /// </summary>
        public MessageEmoteCollection()
        {
            _emoteList = new SortedList<string, MessageEmote>();
            _preferredFilter = AllInclusiveEmoteFilter;
        }

        /// <inheritdoc />
        /// <summary>
        ///     Constructor which specifies a particular preferred <see cref="T:TwitchLib.Models.Client.MessageEmoteCollection.EmoteFilterDelegate" />
        /// </summary>
        /// <param name="preferredFilter"></param>
        public MessageEmoteCollection(EmoteFilterDelegate preferredFilter) : this()
        {
            _preferredFilter = preferredFilter;
        }

        /// <summary>
        ///     Adds an <see cref="MessageEmote"/> to the collection. Duplicate emotes
        ///     (judged by <see cref="MessageEmote.Text"/>) are ignored.
        /// </summary>
        /// <param name="emote">The <see cref="MessageEmote"/> to add to the collection.</param>
        public void Add(MessageEmote emote)
        {
            if (!_emoteList.TryGetValue(emote.Text, out var _))
            {
                _emoteList.Add(emote.Text, emote);
            }


            if (CurrentPattern == null)
            {
                //string i = String.Format(_basePattern, "(" + emote.EscapedText + "){0}");
                CurrentPattern = string.Format(BasePattern, emote.EscapedText);
            }
            else
            {
                CurrentPattern = CurrentPattern + "|" + string.Format(BasePattern, emote.EscapedText);
            }
        }

        /// <summary>
        ///     Adds every <see cref="MessageEmote"/> from an <see cref="IEnumerable{T}">enumerable</see>
        ///     collection to the internal collection.
        ///     Duplicate emotes (judged by <see cref="MessageEmote.Text"/>) are ignored.
        /// </summary>
        /// <param name="emotes">A collection of <see cref="MessageEmote"/>s.</param>
        public void Merge(IEnumerable<MessageEmote> emotes)
        {
            var enumerator = emotes.GetEnumerator();
            while (enumerator.MoveNext())
            {
                Add(enumerator.Current);
            }

            enumerator.Dispose();
        }

        /// <summary>
        ///     Removes the specified <see cref="MessageEmote"/> from the collection.
        /// </summary>
        /// <param name="emote">The <see cref="MessageEmote"/> to remove.</param>
        public void Remove(MessageEmote emote)
        {
            if (!_emoteList.ContainsKey(emote.Text)) return;

            _emoteList.Remove(emote.Text);

            // These patterns look a lot scarier than they are because we have to look for
            // a lot of regex characters, which means we do a lot of escaping!

            // Matches ^(\bEMOTE\b)| and ^(\bEMOTE\b)
            // It's all grouped so that we can OR it with the second pattern.
            var firstEmotePattern = @"(^\(\\b" + emote.EscapedText + @"\\b\)\|?)";
            // Matches |(\bEMOTE\b) including the preceding | so that the following | and emote (if any)
            // merge seamlessly when this section is removed. Again, wrapped in a group.
            var otherEmotePattern = @"(\|\(\\b" + emote.EscapedText + @"\\b\))";
            var newPattern = Regex.Replace(CurrentPattern, firstEmotePattern + "|" + otherEmotePattern, "");
            CurrentPattern = newPattern.Equals("") ? null : newPattern;
        }

        /// <summary>
        ///     Removes all <see cref="MessageEmote"/>s from the collection.
        /// </summary>
        public void RemoveAll()
        {
            _emoteList.Clear();
            CurrentPattern = null;
        }

        /// <summary>
        ///     Replaces all instances of all registered emotes passing the provided
        ///     <see cref="EmoteFilterDelegate"/> with their designated
        ///     <see cref="MessageEmote.ReplacementString"/>s
        /// </summary>
        /// <param name="originalMessage">
        ///     The original message which needs to be processed for emotes.
        /// </param>
        /// <param name="del">
        ///     An <see cref="EmoteFilterDelegate"/> which returns true if its
        ///     received <see cref="MessageEmote"/> is to be replaced.
        ///     Defaults to <see cref="CurrentEmoteFilter"/>.
        /// </param>
        /// <returns>
        ///     A string where all of the original emote text has been replaced with
        ///     its designated <see cref="MessageEmote.ReplacementString"/>s
        /// </returns>
        public string ReplaceEmotes(string originalMessage, EmoteFilterDelegate del = null)
        {
            if (CurrentRegex == null) return originalMessage;
            if (del != null && del != CurrentEmoteFilter) CurrentEmoteFilter = del;
            var newMessage = CurrentRegex.Replace(originalMessage, GetReplacementString);
            CurrentEmoteFilter = _preferredFilter;
            return newMessage;
        }

        /// <summary>
        ///     A delegate function which, when given a <see cref="MessageEmote"/>,
        ///     determines whether it should be replaced.
        /// </summary>
        /// <param name="emote">The <see cref="MessageEmote"/> to be considered</param>
        /// <returns>true if the <see cref="MessageEmote"/> should be replaced.</returns>
        public delegate bool EmoteFilterDelegate(MessageEmote emote);

        /// <summary>
        ///     The default emote filter includes every <see cref="MessageEmote"/> registered on this list.
        /// </summary>
        /// <param name="emote">An emote which is ignored in this filter.</param>
        /// <returns>true always</returns>
        public static bool AllInclusiveEmoteFilter(MessageEmote emote)
        {
            return true;
        }

        /// <summary>
        ///     This emote filter includes only <see cref="MessageEmote"/>s provided by Twitch.
        /// </summary>
        /// <param name="emote">
        ///     A <see cref="MessageEmote"/> which will be replaced if its
        ///     <see cref="MessageEmote.Source">Source</see> is <see cref="MessageEmote.EmoteSource.Twitch"/>
        /// </param>
        /// <returns>true always</returns>
        public static bool TwitchOnlyEmoteFilter(MessageEmote emote)
        {
            return emote.Source == MessageEmote.EmoteSource.Twitch;
        }

        private string GetReplacementString(Match m)
        {
            if (!_emoteList.ContainsKey(m.Value)) return m.Value;

            var emote = _emoteList[m.Value];
            return CurrentEmoteFilter(emote) ? emote.ReplacementString : m.Value;
            //If the match doesn't exist in the list ("shouldn't happen") or the filter excludes it, don't replace.
        }
    }
}
