﻿namespace Sanakan.Common.Models.TwitchModels
{
    public readonly struct HostingStopped
    {
        public string HostingChannel { get; }
        public int Viewers { get; }

        public HostingStopped(IrcMessage ircMessage)
        {
            var splitted = ircMessage.Message.Split(' ');
            HostingChannel = ircMessage.Channel;
            Viewers = splitted[1].StartsWith("-") ? 0 : int.Parse(splitted[1]);
        }
        
    }
}
