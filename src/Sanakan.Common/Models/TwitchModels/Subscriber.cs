﻿using System.Collections.Generic;
using System.Drawing;

namespace Sanakan.Common.Models.TwitchModels
{
    public class Subscriber : SubscriberBase
    {
        public Subscriber(IrcMessage ircMessage) 
            : base(ircMessage)
        {
        }
    }
}
