﻿namespace Sanakan.Common.Models.TwitchModels
{
    public readonly struct HostingStarted
    {
        public string HostingChannel { get; }
        public string TargetChannel { get; }
        public int Viewers { get; }

        public HostingStarted(IrcMessage ircMessage)
        {
            var splitted = ircMessage.Message.Split(' ');
            HostingChannel = ircMessage.Channel;
            TargetChannel = splitted[0];
            Viewers = splitted[1].StartsWith("-") ? 0 : int.Parse(splitted[1]);
        }
        public HostingStarted(string hostingChannel, string targetChannel, int viewers) =>
            (HostingChannel, TargetChannel, Viewers) = ( hostingChannel, targetChannel, viewers);
    }
}
