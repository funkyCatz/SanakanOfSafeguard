﻿using Sanakan.Common.Models.TwitchModels;

namespace Sanakan.Common.Models.TwitchModels
{
    public readonly struct CheerBadge
    {
        /// <summary>Property representing raw cheer amount represented by badge.</summary>
        public int CheerAmount { get; }
        /// <summary>Property representing the color of badge via an enum.</summary>
        public BadgeColorEnum Color { get; }

        /// <summary>Constructor for CheerBadge</summary>
        public CheerBadge(int cheerAmount) : this()
        {
            CheerAmount = cheerAmount;
            Color = getColor(cheerAmount);
        }

        private BadgeColorEnum getColor(int cheerAmount)
        {
            if (cheerAmount >= 10000)
                return BadgeColorEnum.Red;
            if (cheerAmount >= 5000)
                return BadgeColorEnum.Blue;
            if (cheerAmount >= 1000)
                return BadgeColorEnum.Green;
            return cheerAmount >= 100 ? BadgeColorEnum.Purple : BadgeColorEnum.Gray;
        }
    }
}
