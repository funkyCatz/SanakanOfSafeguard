﻿using System.Collections.Generic;
using System.Linq;

namespace Sanakan.Common.Models.TwitchModels
{
    public class ChatCommand
    {
        public List<string> ArgumentsAsList { get; }
        /// <summary>Property representing all arguments received in a string form.</summary>
        public string ArgumentsAsString { get; }
        /// <summary>Property representing the chat message that the command came in.</summary>
        public ChatMessage ChatMessage { get; }
        /// <summary>Property representing the command identifier (ie command prefix).</summary>
        public char CommandIdentifier { get; }
        /// <summary>Property representing the actual command (without the command prefix).</summary>
        public string CommandName { get; }

        /// <summary>ChatCommand constructor.</summary>
        /// <param name="chatMessage"></param>
        public ChatCommand(ChatMessage chatMessage)
        {
            ChatMessage = chatMessage;
            CommandName = chatMessage.Message.Split(' ')[0].Substring(1, chatMessage.Message.Split(' ')[0].Length - 1);
            ArgumentsAsString = chatMessage.Message.Contains(" ") ? chatMessage.Message.Replace(
                $"{chatMessage.Message.Split(' ')[0]} ", "") : "";
            if (!chatMessage.Message.Contains("\"") || chatMessage.Message.Count(x => x == '"') % 2 == 1)
                ArgumentsAsList = chatMessage.Message.Split(' ')?.Where(arg => chatMessage.Message != null && arg !=
                    $"{chatMessage.Message[0]}{CommandName}").ToList();
            else
                ArgumentsAsList = ParseQuotesAndNonQuotes(ArgumentsAsString);
            CommandIdentifier = chatMessage.Message[0];
        }

        private List<string> ParseQuotesAndNonQuotes(string message)
        {
            var args = new List<string>();

            // Return if empty string
            if (message == "")
                return new List<string>();

            var previousQuoted = message[0] != '"';
            // Parse quoted text as a single argument
            foreach (var arg in message.Split('"'))
            {
                if (string.IsNullOrEmpty(arg))
                    continue;

                // This arg is a quoted arg, add it right away
                if (!previousQuoted)
                {
                    args.Add(arg);
                    previousQuoted = true;
                    continue;
                }

                if (!arg.Contains(" "))
                    continue;

                // This arg is non-quoted, iterate through each split and add it if it's not empty/whitespace
                foreach (var dynArg in arg.Split(' '))
                {
                    if (string.IsNullOrWhiteSpace(dynArg))
                        continue;

                    args.Add(dynArg);
                    previousQuoted = false;
                }
            }
            return args;
        }
    }
}
