﻿using System;

namespace Sanakan.Common.Models.TwitchModels
{
    public readonly struct UserBan
    {
        public string BanReason { get; }
        public string Channel { get; }
        public string Username { get; }


        public UserBan(IrcMessage ircMessage)
        {
            Channel = ircMessage.Channel;
            Username = ircMessage.Message;
            BanReason = String.Empty;

            var successBanReason = ircMessage.Tags.TryGetValue(Tags.BanReason, out var banReason);
            if (successBanReason)
            {
                BanReason = banReason;
            }
        }
    }
}
