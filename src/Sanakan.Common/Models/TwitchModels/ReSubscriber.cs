﻿using System.Collections.Generic;
using System.Drawing;

namespace Sanakan.Common.Models.TwitchModels
{
    public class ReSubscriber : SubscriberBase
    {
        public int Months { get; }

        public ReSubscriber(IrcMessage ircMessage)
            : base(ircMessage) => (Months) = (months);
    }
}
