﻿using System.Collections.Generic;

namespace Sanakan.Common.Models.TwitchModels
{
    public class EmoteSet
    {
        public IList<Emote> Emotes { get; }
        public string RawEmoteSetString { get; }

        public EmoteSet(string emoteSetData, string message)
        {
            Emotes = new List<Emote>();
            RawEmoteSetString = emoteSetData;
            if (string.IsNullOrEmpty(emoteSetData))
                return;
            if (emoteSetData.Contains("/"))
            {
                // Message contains multiple different emotes, first parse by unique emotes: 28087:15-21/25:5-9,28-32
                foreach (var emoteData in emoteSetData.Split('/'))
                {
                    var emoteId = int.Parse(emoteData.Split(':')[0]);
                    if (emoteData.Contains(","))
                    {
                        // Multiple copies of a single emote: 25:5-9,28-32
                        foreach (var emote in emoteData.Replace($"{emoteId}:", "").Split(','))
                            AddEmote(emote, emoteId, message);

                    }
                    else
                    {
                        // Single copy of single emote: 25:5-9/28087:16-22
                        AddEmote(emoteData, emoteId, message, true);
                    }
                }
            }
            else
            {
                var emoteId = int.Parse(emoteSetData.Split(':')[0]);
                // Message contains a single, or multiple of the same emote
                if (emoteSetData.Contains(","))
                {
                    // Multiple copies of a single emote: 25:5-9,28-32
                    foreach (var emote in emoteSetData.Replace($"{emoteId}:", "").Split(','))
                        AddEmote(emote, emoteId, message);
                }
                else
                {
                    // Single copy of single emote: 25:5-9
                    AddEmote(emoteSetData, emoteId, message, true);
                }
            }
        }

        private void AddEmote(string emoteData, int emoteId, string message, bool single = false)
        {
            int startIndex, endIndex;
            if (single)
            {
                startIndex = int.Parse(emoteData.Split(':')[1].Split('-')[0]);
                endIndex = int.Parse(emoteData.Split(':')[1].Split('-')[1]);
            }
            else
            {
                startIndex = int.Parse(emoteData.Split('-')[0]);
                endIndex = int.Parse(emoteData.Split('-')[1]);
            }
            Emotes.Add(new Emote(emoteId, message.Substring(startIndex, (endIndex - startIndex) + 1), startIndex, endIndex));
        }
    }
}
