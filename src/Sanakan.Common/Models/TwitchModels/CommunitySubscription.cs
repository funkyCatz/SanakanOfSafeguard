﻿using System;
using System.Runtime.CompilerServices;

namespace Sanakan.Common.Models.TwitchModels
{
    public readonly struct CommunitySubscription
    {
        public string Badges { get; }
        public string Color { get; }
        public string DisplayName { get; }
        public string Emotes { get; }
        public string Id { get; }
        public string Login { get; }
        public bool IsModerator { get; }
        public string MsgId { get; }
        public int MsgParamMassGiftCount { get; }
        public int MsgParamSenderCount { get; }
        public SubscriptionPlanEnum MsgParamSubPlan { get; }
        public string RoomId { get; }
        public bool IsSubscriber { get; }
        public string SystemMsg { get; }
        public string SystemMsgParsed { get; }
        public string TmiSentTs { get; }
        public bool IsTurbo { get; }
        public string UserId { get; }
        public UserTypeEnum UserType { get; }

        public CommunitySubscription(IrcMessage ircMessage) : this()
        {
            foreach (var tag in ircMessage.Tags.Keys)
            {
                var tagValue = ircMessage.Tags[tag];

                switch (tag)
                {
                    case Tags.Badges:
                        Badges = tagValue;
                        break;
                    case Tags.Color:
                        Color = tagValue;
                        break;
                    case Tags.DisplayName:
                        DisplayName = tagValue;
                        break;
                    case Tags.Emotes:
                        Emotes = tagValue;
                        break;
                    case Tags.Id:
                        Id = tagValue;
                        break;
                    case Tags.Login:
                        Login = tagValue;
                        break;
                    case Tags.Mod:
                        IsModerator = ConvertToBool(tagValue);
                        break;
                    case Tags.MsgId:
                        MsgId = tagValue;
                        break;
                    case Tags.MsgParamSubPlan:
                        switch (tagValue)
                        {
                            case "prime":
                                MsgParamSubPlan = SubscriptionPlanEnum.Prime;
                                break;
                            case "1000":
                                MsgParamSubPlan = SubscriptionPlanEnum.Tier1;
                                break;
                            case "2000":
                                MsgParamSubPlan = SubscriptionPlanEnum.Tier2;
                                break;
                            case "3000":
                                MsgParamSubPlan = SubscriptionPlanEnum.Tier3;
                                break;
                            default:
                                throw new ArgumentOutOfRangeException(nameof(tagValue.ToLower));
                        }
                        break;
                    case Tags.MsgParamMassGiftCount:
                        MsgParamMassGiftCount = int.Parse(tagValue);
                        break;
                    case Tags.MsgParamSenderCount:
                        MsgParamSenderCount = int.Parse(tagValue);
                        break;
                    case Tags.RoomId:
                        RoomId = tagValue;
                        break;
                    case Tags.Subscriber:
                        IsSubscriber = ConvertToBool(tagValue);
                        break;
                    case Tags.SystemMsg:
                        SystemMsg = tagValue;
                        SystemMsgParsed = tagValue.Replace("\\s", " ").Replace("\\n", "");
                        break;
                    case Tags.TmiSentTs:
                        TmiSentTs = tagValue;
                        break;
                    case Tags.Turbo:
                        IsTurbo = ConvertToBool(tagValue);
                        break;
                    case Tags.UserType:
                        switch (tagValue)
                        {
                            case "mod":
                                UserType = UserTypeEnum.Moderator;
                                break;
                            case "global_mod":
                                UserType = UserTypeEnum.GlobalModerator;
                                break;
                            case "admin":
                                UserType = UserTypeEnum.Admin;
                                break;
                            case "staff":
                                UserType = UserTypeEnum.Staff;
                                break;
                            default:
                                UserType = UserTypeEnum.Viewer;
                                break;
                        }
                        break;
                }
            }
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private bool ConvertToBool(string data) => data == "1";
    }
}
