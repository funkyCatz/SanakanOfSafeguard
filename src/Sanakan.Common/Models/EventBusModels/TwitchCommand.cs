﻿using System;
using MediatR;

namespace Sanakan.Common.Models.EventBusModels
{
    public class TwitchCommand : INotification
    {
        public string Value { get; }
        
        public DateTime TimeStamp { get; }

        public TwitchCommand(string value)
        {
            Value = value;
            TimeStamp = DateTime.UtcNow;
        }
    }
}