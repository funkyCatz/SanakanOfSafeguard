﻿using MediatR;

namespace Sanakan.Common.Models.EventBusModels
{
    public class TwitchQuery : INotification
    {
        public string Value { get; }

        public TwitchQuery(string value)
        {
            Value = value;
        }
    }
}