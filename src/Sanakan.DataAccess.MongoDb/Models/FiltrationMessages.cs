﻿using System;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using Sanakan.DataAccess.Domain.Entities;
using Sanakan.DataAccess.Mapping.Interfaces;

namespace Sanakan.DataAccess.MongoDb.Models
{
    public class FiltrationMessages : IMapFrom<FiltrationMessage>
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }
        public int UserId { get; set; }
        public string Author { get; set; }
        public string Text { get; set; }
        public DateTime TimeStamp { get; set; }
        public string Channel { get; set; }
    }
}