﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.Extensions.Options;
using MongoDB.Driver;
using Sanakan.DataAccess.Domain.Entities;
using Sanakan.DataAccess.MongoDb.Models;
using Sanakan.DataAccess.Options;
using Sanakan.DataAccess.Services.Interfaces;

namespace Sanakan.DataAccess.MongoDb.Services
{
    public class UserEventLogsService : IUserEventLogsService
    {
        private readonly IMapper _mapper;
        private readonly IMongoCollection<UserEventLogs> _userEventLogsCollection;

        public UserEventLogsService(IMapper mapper, 
            IOptions<DataBaseOptions> dbOption,
            IMongoClient mongoClient)
        {
            _mapper = mapper;
            var db = mongoClient.GetDatabase(dbOption.Value.DatabaseName);
            _userEventLogsCollection = db.GetCollection<UserEventLogs>(nameof(UserEventLogs));
        }
        
        public Task AddAsync(UserEventLog log, CancellationToken stoppingToken)
        {
            return _userEventLogsCollection.InsertOneAsync(_mapper.Map<UserEventLogs>(log), null, stoppingToken);
        }

        public async Task<List<UserEventLog>> GetAsync(CancellationToken stoppingToken)
        {
            var cursor = await _userEventLogsCollection.FindAsync(FilterDefinition<UserEventLogs>.Empty, 
                null, stoppingToken);
            var entities = await cursor.ToListAsync(stoppingToken);

            return _mapper.Map<List<UserEventLog>>(entities);
        }

        public async Task<List<UserEventLog>> GetAsync(Expression<Func<UserEventLog, bool>> exp, CancellationToken stoppingToken)
        {
            var exprTree = _mapper.Map<Expression<Func<UserEventLogs, bool>>>(exp);
            var cursor = await _userEventLogsCollection.FindAsync(exprTree, 
                null, stoppingToken);
            var entities = await cursor.ToListAsync(stoppingToken);

            return _mapper.Map<List<UserEventLog>>(entities);
        }
    }
}