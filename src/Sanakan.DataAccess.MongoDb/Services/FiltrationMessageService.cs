﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Options;
using Microsoft.Extensions.Primitives;
using Sanakan.DataAccess.Cache.Interfaces;
using Sanakan.DataAccess.Domain.Entities;
using Sanakan.DataAccess.Services.Interfaces;
using MongoDB.Driver;
using Sanakan.DataAccess.MongoDb.Models;
using Sanakan.DataAccess.Options;

namespace Sanakan.DataAccess.MongoDb.Services
{
    public class FiltrationMessageService: IFiltrationMessageService
    {
        private readonly ISignal _signal;
        private readonly IMemoryCache _memoryCache;
        private readonly IMapper _mapper;
        private readonly IMongoCollection<FiltrationMessages> _filtrationCollection;

        public FiltrationMessageService(ISignal signal,
            IMemoryCache memoryCache,
            IMapper mapper,
            IOptions<DataBaseOptions> dbOption, IMongoClient mongoClient)
        {
            _signal = signal;
            _memoryCache = memoryCache;
            _mapper = mapper;
            var db = mongoClient.GetDatabase(dbOption.Value.DatabaseName);
            _filtrationCollection = db.GetCollection<FiltrationMessages>(nameof(FiltrationMessages));
        }
        
        private IChangeToken ChangeToken => _signal.GetToken(nameof(FiltrationMessage));
        
        public async Task AddAsync(FiltrationMessage message, CancellationToken stoppingToken)
        {
            var storedEntities = await GetAsync(message.Channel, stoppingToken);

            if (!storedEntities.Any( x => string.Equals(x.Text, message.Text)))
            {
                await _filtrationCollection.InsertOneAsync(_mapper.Map<FiltrationMessages>(message),
                    null, stoppingToken);
            
                _signal.SignalToken(nameof(FiltrationMessage));
            }
        }

        public async Task<List<FiltrationMessage>> GetAsync(CancellationToken stoppingToken)
        {
            return await _memoryCache.GetOrCreateAsync(nameof(FiltrationMessage), async entry =>
            {
                var cursor = await _filtrationCollection.FindAsync(FilterDefinition<FiltrationMessages>.Empty, 
                    null, stoppingToken);
                var entities = await cursor.ToListAsync(stoppingToken);
                entry.ExpirationTokens.Add(ChangeToken);

                return _mapper.Map<List<FiltrationMessage>>(entities);
            });
        }

        public async Task<List<FiltrationMessage>> GetAsync(string channelName, CancellationToken stoppingToken)
        {
            return await _memoryCache.GetOrCreateAsync(nameof(FiltrationMessage), async entry =>
            {
                var cursor = await _filtrationCollection.FindAsync(filter => string.Equals(filter.Channel, channelName), 
                    null, stoppingToken);
                var entities = await cursor.ToListAsync(stoppingToken);
                entry.ExpirationTokens.Add(ChangeToken);

                return _mapper.Map<List<FiltrationMessage>>(entities);
            });
        }

        public async Task DeleteAsync(string message, string channel, CancellationToken stoppingToken)
        {
            await _filtrationCollection.DeleteManyAsync(filter =>
                string.Equals(filter.Channel, channel, StringComparison.InvariantCulture) &&
                string.Equals(filter.Text, message, StringComparison.InvariantCulture), stoppingToken);
            
            _signal.SignalToken(nameof(FiltrationMessage));
        }
    }
}