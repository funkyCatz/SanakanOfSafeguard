﻿using System.Reflection;
using System.Security.Cryptography.X509Certificates;
using AutoMapper;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using MongoDB.Driver;
using Sanakan.DataAccess.MongoDb.Services;
using Sanakan.DataAccess.Services.Interfaces;

namespace Sanakan.DataAccess.MongoDb
{
    public static class DependencyInjection
    {
        public static void RegisterMongoDb(this IServiceCollection services, HostBuilderContext hostContext)
        {
            var dataBaseSection = hostContext.Configuration.GetSection("DataBase");
            if (!string.IsNullOrWhiteSpace(dataBaseSection["CertPath"]))
            {
                MongoClientSettings settings = new MongoClientSettings
                {
                    Server = MongoServerAddress.Parse(dataBaseSection["DatabaseUrl"]),
                    SslSettings = new SslSettings
                    {
                        ClientCertificates = new[] { new X509Certificate2(dataBaseSection["CertPath"]) }
                    }
                };

                services.AddSingleton<IMongoClient>(new MongoClient(settings));
            }
            else
            {
                services.AddSingleton<IMongoClient>(new MongoClient(dataBaseSection["DatabaseUrl"]));
            }
            
            services.AddAutoMapper(Assembly.GetExecutingAssembly());

            services.AddSingleton<IUserEventLogsService, UserEventLogsService>();
            services.AddSingleton<IFiltrationMessageService, FiltrationMessageService>();
        }
    }
}