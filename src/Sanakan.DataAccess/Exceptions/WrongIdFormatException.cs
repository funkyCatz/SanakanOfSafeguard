﻿using System;

namespace Sanakan.DataAccess.Exceptions
{
    public class WrongIdFormatException : Exception
    {
        public WrongIdFormatException(string Id) : base(message: $"Wrong Id format \'{Id}\'") { }
    }
}