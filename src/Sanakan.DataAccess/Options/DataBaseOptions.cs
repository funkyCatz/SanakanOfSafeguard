﻿namespace Sanakan.DataAccess.Options
{
    public class DataBaseOptions
    {
        public string DataBaseType { get; set; }
        public string DatabaseUrl { get; set; }
        public string DatabaseName { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
        
        public string CertPath { get; set; }
    }
}