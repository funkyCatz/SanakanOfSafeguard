﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Primitives;
using Sanakan.DataAccess.Cache.Interfaces;
using Sanakan.DataAccess.Domain.Entities;
using Sanakan.DataAccess.Repository.Interfaces;

namespace Sanakan.DataAccess.Repository
{
    public class AsyncCacheableRepository<TEntity> 
        : IAsyncCacheableRepository<TEntity> where TEntity : Identity
    {
        private readonly ISignal _signal;
        private readonly IMemoryCache _memoryCache;
        private readonly IAsyncRepository<TEntity> _unwrappedRepository;
        private readonly ILogger<AsyncCacheableRepository<TEntity>> _logger;

        public AsyncCacheableRepository(ISignal signal, 
            IMemoryCache memoryCache,
            IAsyncRepository<TEntity> unwrappedRepository,
            ILogger<AsyncCacheableRepository<TEntity>> logger)
        {
            _signal = signal;
            _memoryCache = memoryCache;
            _unwrappedRepository = unwrappedRepository;
            _logger = logger;
        }
        
        private IChangeToken ChangeToken => _signal.GetToken(nameof(TEntity));
        
        public async Task UpdateAsync(TEntity newObj, CancellationToken stoppingToken = default)
        {
            try
            {
                await _unwrappedRepository.UpdateAsync(newObj, stoppingToken);
                _signal.SignalToken(nameof(TEntity));
            }
            catch (Exception e)
            {
                _logger.LogError(e, e.Message);
            }
        }

        public async Task<TEntity> GetAsync(string Id, CancellationToken stoppingToken = default)
        {
            return await _unwrappedRepository.GetAsync(Id, stoppingToken);
        }

        public async Task<IEnumerable<TEntity>> GetAllAsync(CancellationToken stoppingToken = default)
        {
            try
            {
                return await _memoryCache.GetOrCreateAsync(nameof(TEntity), async (entry) =>
                {
                    var entities = await _unwrappedRepository.GetAllAsync(stoppingToken);
                
                    entry.ExpirationTokens.Add(ChangeToken);

                    return entities;
                });
            }
            catch(Exception e)
            {
                _logger.LogError(e, e.Message);
                throw;
            }
        }

        public async Task<IEnumerable<TEntity>> FindAsync(Expression<Func<TEntity, bool>> predicate, CancellationToken stoppingToken = default)
        {
            try
            {
                return await _unwrappedRepository.FindAsync(predicate, stoppingToken);
            }
            catch (Exception e)
            {
                _logger.LogError(e, e.Message);
                throw;
            }
        }

        public Task DeleteAsync(string Id, CancellationToken stoppingToken = default)
        {
            try
            {
                _signal.SignalToken(nameof(TEntity));
                return _unwrappedRepository.DeleteAsync(Id, stoppingToken);
            }
            catch (Exception e)
            {
                _logger.LogError(e, e.Message);
                throw;
            }
        }

        public Task DeleteAsync(IAsyncEnumerable<string> ids, CancellationToken stoppingToken = default)
        {
            try
            {
                _signal.SignalToken(nameof(TEntity));
                return _unwrappedRepository.DeleteAsync(ids, stoppingToken);
            }
            catch (Exception e)
            {
                _logger.LogError(e, e.Message);
                throw;
            }
        }

        public Task DeleteAsync(IEnumerable<string> ids, CancellationToken stoppingToken = default)
        {
            try
            {
                _signal.SignalToken(nameof(TEntity));
                return _unwrappedRepository.DeleteAsync(ids, stoppingToken);
            }
            catch (Exception e)
            {
                _logger.LogError(e, e.Message);
                throw;
            }
        }

        public Task DeleteAsync(TEntity entity, CancellationToken stoppingToken = default)
        {
            try
            {
                _signal.SignalToken(nameof(TEntity));
                return _unwrappedRepository.DeleteAsync(entity, stoppingToken);
            }
            catch (Exception e)
            {
                _logger.LogError(e, e.Message);
                throw;
            }
        }
    }
}