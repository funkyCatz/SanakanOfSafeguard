﻿using Sanakan.DataAccess.Domain.Entities;

namespace Sanakan.DataAccess.Repository.Interfaces
{
    public interface IAsyncCacheableRepository<TEntity> 
        : IAsyncRepository<TEntity> where TEntity : Identity
    {
        
    }
}