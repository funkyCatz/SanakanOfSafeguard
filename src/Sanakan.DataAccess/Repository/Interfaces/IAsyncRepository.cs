﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using Sanakan.DataAccess.Domain.Entities;

namespace Sanakan.DataAccess.Repository.Interfaces
{
    public interface IAsyncRepository<TEntity> 
        where TEntity : Identity
    {
        Task UpdateAsync(TEntity newObj, CancellationToken stoppingToken = default);
        Task<TEntity> GetAsync(string Id, CancellationToken stoppingToken = default);
        Task<IEnumerable<TEntity>> GetAllAsync(CancellationToken stoppingToken = default);
        Task<IEnumerable<TEntity>> FindAsync(Expression<Func<TEntity, bool>> predicate, CancellationToken stoppingToken = default);
        Task DeleteAsync(string Id, CancellationToken stoppingToken = default);
        Task DeleteAsync(IAsyncEnumerable<string> ids, CancellationToken stoppingToken = default);
        Task DeleteAsync(IEnumerable<string> ids, CancellationToken stoppingToken = default);
        Task DeleteAsync(TEntity entity, CancellationToken stoppingToken = default);
    }
}