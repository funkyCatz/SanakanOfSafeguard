﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Sanakan.DataAccess.Domain.Entities;

namespace Sanakan.DataAccess.Services.Interfaces
{
    public interface IFiltrationMessageService
    {
        Task AddAsync(FiltrationMessage message, CancellationToken stoppingToken);
        Task<List<FiltrationMessage>> GetAsync(CancellationToken stoppingToken);
        Task<List<FiltrationMessage>> GetAsync(string channelName, CancellationToken stoppingToken);
        Task DeleteAsync(string message, string channel, CancellationToken stoppingToken);
    }
}