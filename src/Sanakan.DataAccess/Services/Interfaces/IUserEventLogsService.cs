﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using Sanakan.DataAccess.Domain.Entities;

namespace Sanakan.DataAccess.Services.Interfaces
{
    public interface IUserEventLogsService
    {
        Task AddAsync(UserEventLog log, CancellationToken stoppingToken);
        Task<List<UserEventLog>> GetAsync(CancellationToken stoppingToken);
        Task<List<UserEventLog>> GetAsync(Expression<Func<UserEventLog, bool>> exp, CancellationToken stoppingToken);
    }
}