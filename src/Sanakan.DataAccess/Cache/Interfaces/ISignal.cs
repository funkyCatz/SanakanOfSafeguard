﻿using Microsoft.Extensions.Primitives;

namespace Sanakan.DataAccess.Cache.Interfaces
{
    public interface ISignal
    {
        IChangeToken GetToken(string key);

        void SignalToken(string key);

    }
}