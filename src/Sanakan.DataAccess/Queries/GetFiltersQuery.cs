﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Sanakan.DataAccess.Domain.Entities;
using Sanakan.DataAccess.Services.Interfaces;

namespace Sanakan.DataAccess.Queries
{
    public class GetFiltersQuery : IRequest<List<FiltrationMessage>>
    {
        public string ChannelName { get; set; }
    }

    public class GetFiltersQueryHandler : IRequestHandler<GetFiltersQuery, List<FiltrationMessage>>
    {
        private readonly IFiltrationMessageService _filtrationMessageService;

        public GetFiltersQueryHandler(IFiltrationMessageService filtrationMessageService)
        {
            _filtrationMessageService = filtrationMessageService;
        }
        
        public Task<List<FiltrationMessage>> Handle(GetFiltersQuery request,
            CancellationToken cancellationToken)
        {
            return _filtrationMessageService.GetAsync(request.ChannelName, cancellationToken);
        }
    }
}