﻿using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Sanakan.DataAccess.Domain.Entities;
using Sanakan.DataAccess.Services.Interfaces;

namespace Sanakan.DataAccess.Commands
{
    public class DeleteFiltrationMessageCommand: IRequest
    {
        public FiltrationMessage FiltrationMessage { get; set; }
    }

    public class DeleteFiltrationMessageCommandHandler : IRequestHandler<DeleteFiltrationMessageCommand>
    {
        private readonly IFiltrationMessageService _filtrationMessageService;

        public DeleteFiltrationMessageCommandHandler(IFiltrationMessageService filtrationMessageService)
        {
            _filtrationMessageService = filtrationMessageService;
        }
        
        public async Task<Unit> Handle(DeleteFiltrationMessageCommand request, CancellationToken cancellationToken)
        {
            await _filtrationMessageService.DeleteAsync(request.FiltrationMessage.Text, request.FiltrationMessage.Channel,
                cancellationToken);
            return Unit.Value;
        }
    }
}