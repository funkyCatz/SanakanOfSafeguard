﻿using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Sanakan.DataAccess.Domain.Entities;
using Sanakan.DataAccess.Services.Interfaces;

namespace Sanakan.DataAccess.Commands
{
    public class AddFiltrationMessageCommand : IRequest
    {
        public FiltrationMessage FiltrationMessage { get; set; } = null!;
    }

    public class AddFiltrationMessageCommandHandler : IRequestHandler<AddFiltrationMessageCommand>
    {
        private readonly IFiltrationMessageService _filtrationMessageService;

        public AddFiltrationMessageCommandHandler(IFiltrationMessageService filtrationMessageService)
        {
            _filtrationMessageService = filtrationMessageService;
        }
        
        public async Task<Unit> Handle(AddFiltrationMessageCommand request, CancellationToken cancellationToken)
        {
            await _filtrationMessageService.AddAsync(request.FiltrationMessage, cancellationToken);
            return Unit.Value;
        }
    }
}