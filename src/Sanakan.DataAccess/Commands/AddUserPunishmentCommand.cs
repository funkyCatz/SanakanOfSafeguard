﻿using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Sanakan.DataAccess.Domain.Entities;
using Sanakan.DataAccess.Services.Interfaces;

namespace Sanakan.DataAccess.Commands
{
    public class AddUserPunishmentCommand : IRequest
    {
        public UserEventLog UserPunishment { get; set; } = null!;
    }

    public class AddUserPunishmentCommandHandler : IRequestHandler<AddUserPunishmentCommand>
    {
        private readonly IUserEventLogsService _userEventLogsService;
        public AddUserPunishmentCommandHandler(IUserEventLogsService userEventLogsService)
        {
            _userEventLogsService = userEventLogsService;
        }
        
        public async Task<Unit> Handle(AddUserPunishmentCommand request, CancellationToken cancellationToken)
        {
            await _userEventLogsService.AddAsync(request.UserPunishment, cancellationToken);
            return Unit.Value;
        }
    }
}