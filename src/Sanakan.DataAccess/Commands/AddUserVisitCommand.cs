﻿using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Sanakan.DataAccess.Domain.Entities;
using Sanakan.DataAccess.Services.Interfaces;

namespace Sanakan.DataAccess.Commands
{
    public class AddUserVisitCommand : IRequest
    {
        public UserEventLog UserVisit { get; set; }
    }

    public class AddUserVisitCommandHandler : IRequestHandler<AddUserVisitCommand>
    {
        private readonly IUserEventLogsService _userEventLogsService;
        public AddUserVisitCommandHandler(IUserEventLogsService userEventLogsService)
        {
            _userEventLogsService = userEventLogsService;
        }

        
        public async Task<Unit> Handle(AddUserVisitCommand request, CancellationToken cancellationToken)
        {
            await _userEventLogsService.AddAsync(request.UserVisit, cancellationToken);
            return Unit.Value;
        }
    }
}