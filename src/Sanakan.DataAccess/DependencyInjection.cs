﻿using System.Reflection;
using MediatR;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.DependencyInjection;
using Sanakan.DataAccess.Cache;
using Sanakan.DataAccess.Cache.Interfaces;

namespace Sanakan.DataAccess
{
    public static class DependencyInjection
    {
        public static void RegisterDataAccess(this IServiceCollection services)
        {
            services.AddSingleton<IMemoryCache, MemoryCache>();
            services.AddSingleton<ISignal, Signal>();
            services.AddMediatR(Assembly.GetExecutingAssembly());
        }
    }
}