﻿using AutoMapper;

namespace Sanakan.DataAccess.Mapping.Interfaces
{
    public interface IMapFrom<T>
    {   
        void Mapping(Profile profile) => profile.CreateMap(typeof(T), GetType());
    }
}