﻿using System;

namespace Sanakan.DataAccess.Domain.Entities
{
    public class UserEventLog
    {
        public string UserName { get; set; } = null!;

        public UserEventEnum Event { get; set; }
        
        public string Channel { get; set; } = null!;

        public DateTime TimeStamp { get; set; }
        
        public int UserId { get; set; }
        
        public string RawInfo { get; set; } = null!;
    }
}