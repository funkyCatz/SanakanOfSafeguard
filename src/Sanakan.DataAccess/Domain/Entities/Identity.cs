﻿namespace Sanakan.DataAccess.Domain.Entities
{
    public abstract class Identity
    {
        public string Id { get; set; } = null!;
    }
}