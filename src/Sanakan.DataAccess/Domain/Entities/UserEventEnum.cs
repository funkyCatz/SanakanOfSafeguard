﻿namespace Sanakan.DataAccess.Domain.Entities
{
    public enum UserEventEnum
    {
        Info,
        Timeout,
        Ban,
        Unban,
        Join,
        Left,
    }
}