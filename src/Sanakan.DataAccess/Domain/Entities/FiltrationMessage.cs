﻿using System;

namespace Sanakan.DataAccess.Domain.Entities
{
    public class FiltrationMessage : Identity
    {
        public int UserId { get; set; }
        public string Author { get; set; }
        public string Text { get; set; }
        public DateTime TimeStamp { get; set; }
        
        public string Channel { get; set; }
    }
}