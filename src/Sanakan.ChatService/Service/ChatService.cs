﻿using System;
using System.Collections.Concurrent;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Sanakan.ChatService.Options;
using Sanakan.ChatService.Service.Interface;
using Sanakan.Common.Models.EventBusModels;

namespace Sanakan.ChatService.Service
{
    public class ChatService : IChatService
    {
        private readonly IChatClient _chatClient;
        private readonly SettingsOption _settingsOptions;
        private readonly ClientOptions _clientOptions;
        private readonly ILogger<ChatService> _logger;
        private readonly IMediator _mediator;
        
        private readonly ConcurrentQueue<Tuple<DateTime, string>> _sendQueue =
            new ConcurrentQueue<Tuple<DateTime, string>>();

        private bool _areNetworkServicesRunning;
        private int _reconnectionAttempt = 0;

        private CancellationToken _stoppingToken;

        private Task[] _networkTasks;
        
        public ChatService(IChatClient client, 
            IOptionsMonitor<SettingsOption> settingsOptions,
            IOptionsMonitor<ClientOptions> clientOptions,
            ILogger<ChatService> logger,
            IMediator mediator)
        {
            _chatClient = client;
            _logger = logger;
            _clientOptions = clientOptions.CurrentValue;
            _settingsOptions = settingsOptions.CurrentValue;
            _mediator = mediator;
        }
        
        #region IChatService Impl
        
        public bool Connected => _chatClient?.Connected ?? false;
        
        public async Task StartAsync(CancellationToken stoppingToken)
        {
            try
            {
                _stoppingToken = stoppingToken;
                await _chatClient.ConnectAsync(_settingsOptions.ServerUrl,
                    _settingsOptions.UseSsl);

                if (Connected)
                {
                    StartNetworkServices();
                }
                
            }
            catch (Exception e)
            {
                _logger.LogError(e, e.Message);
            }
        }

        public async Task StopAsync(CancellationToken stoppingToken = default)
        {
            try
            {
                await _chatClient.CloseConnectionAsync(stoppingToken);
            }
            catch (Exception e)
            {
                _logger.LogError(e, e.Message);
            }
        }

        public void AddMessage(string msg)
        {
            if (!string.IsNullOrWhiteSpace(msg))
            {
                _sendQueue.Enqueue(new Tuple<DateTime, string>(DateTime.UtcNow, msg));
            }
        }
        
        public async Task AddMessageAsync(string msg, CancellationToken stoppingToken = default)
        {
            await Task.Run(() => AddMessage(msg), stoppingToken);
        }

        #endregion

        #region Internal methods Impl

        private void StartNetworkServices()
        {
            _areNetworkServicesRunning = true;
            _networkTasks = new []
            {
                RunListenerTask(),
                RunSenderTask(),
                StartMonitoringTask()
            }.ToArray();

            if (_networkTasks.Any(c => c.IsFaulted))
            {
                _areNetworkServicesRunning = false;
            }
        }

        private async Task RunSenderTask()
        {
            await Task.Run(async () =>
            {
                try
                {
                    while (Connected && _areNetworkServicesRunning)
                    {
                        try
                        {
                            await Task.Delay(_clientOptions.MsgSendDelay, _stoppingToken);
                            if (_sendQueue.TryDequeue(out var message))
                            {
                                await _chatClient.SendMessageAsync(message.Item2, _stoppingToken);
                            }
                        }
                        catch (Exception ex)
                        {
                            _logger.LogError(ex, ex.Message);
                            break;
                        }
                    }
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex, ex.Message);
                }
            }, _stoppingToken);
        }
        
        private async Task RunListenerTask()
        {
            await Task.Run(async () =>
            {
                while (Connected && _areNetworkServicesRunning)
                {
                    try
                    {
                        var message = await _chatClient.ReadMessageAsync();
                        if (!String.IsNullOrWhiteSpace(message))
                        {
                            #if DEBUG
                                _logger.LogInformation(message);
                            #endif
                            await _mediator.Publish(new TwitchQuery(message), _stoppingToken);
                        }
                    }
                    catch (Exception ex)
                    {
                        _logger.LogError(ex, ex.Message);
                    }
                }
            }, _stoppingToken);
        }
        
        private Task StartMonitoringTask()
        {
            return Task.Run(async () =>
            {
                try
                {
                    var currentConnectionState = Connected;
                    while (!_stoppingToken.IsCancellationRequested)
                    {
                        if (currentConnectionState == Connected)
                        {
                            Thread.Sleep(200);
                            continue;
                        }

                        if (!Connected)
                        {
                            if (currentConnectionState && IsReconnectionNeeded())
                            {
                                await ReconnectAsync();
                                return;
                            }
                        }

                        currentConnectionState = Connected;
                    }
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex, ex.Message);
                }

            }, _stoppingToken);
        }


        private async Task ReconnectAsync()
        {
            await _chatClient.CloseConnectionAsync(_stoppingToken);
            await _chatClient.ConnectAsync(_settingsOptions.ServerUrl,
                _settingsOptions.UseSsl);
            
            _logger.LogInformation("ChatService was reconnected.");
        }
        
        private bool IsReconnectionNeeded()
        {
            Interlocked.Increment( ref _reconnectionAttempt);
            return _clientOptions.ReconnectionAttempts >= _reconnectionAttempt;
        }

        #endregion


        #region IDisposable Impl

        public void Dispose()
        {
            _chatClient.Dispose();
        }

        public async ValueTask DisposeAsync()
        {
            await _chatClient.DisposeAsync();
        }

        #endregion
    }
}