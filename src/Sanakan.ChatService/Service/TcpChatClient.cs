﻿using System;
using System.IO;
using System.Net.Security;
using System.Net.Sockets;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Sanakan.ChatService.Service.Interface;

namespace Sanakan.ChatService.Service
{
    public class TcpChatClient : IChatClient
    {
        private readonly TcpClient _tcpClient;
        private StreamReader _streamReader;
        private StreamWriter _streamWriter;

        private readonly ILogger<TcpChatClient> _logger;

        public TcpChatClient(ILogger<TcpChatClient> logger)
        {
            _logger = logger;
            _tcpClient = new TcpClient();
        }

        public async Task ConnectAsync(string serverUrl, bool useSsl)
        {
            try
            {
                if (!Connected)
                {
                    await _tcpClient.ConnectAsync(serverUrl, useSsl ? 6697 : 6667);
                    
                    if (useSsl)
                    {
                        var sslStream = new SslStream(_tcpClient.GetStream());
                        await sslStream.AuthenticateAsClientAsync(serverUrl);
                        _streamReader = new StreamReader(sslStream);
                        _streamWriter = new StreamWriter(sslStream);
                    }
                    else
                    {
                        _streamReader = new StreamReader(_tcpClient.GetStream());
                        _streamWriter = new StreamWriter(_tcpClient.GetStream());
                    }
                }
            }
            catch (Exception e)
            { 
                _logger.LogError(e, e.Message);
            }
        }

        public void Connect(string serverUrl, bool useSsl)
        {
            var task = Task.Run(async () => await ConnectAsync(serverUrl, useSsl));
            task.Wait();
        }

        public async Task<string> ReadMessageAsync()
        {
            return await _streamReader.ReadLineAsync();
        }

        public async Task SendMessageAsync(string message, CancellationToken stoppingToken = default)
        {
            await _streamWriter.WriteLineAsync(message);
            await _streamWriter.FlushAsync();
        }

        public async Task CloseConnectionAsync(CancellationToken stoppingToken = default)
        {
            await Task.Run(CloseConnection, stoppingToken);
        }

        public void CloseConnection()
        {
            if (Connected)
            {
                _tcpClient?.Close();
            }
        }

        public bool Connected => _tcpClient?.Connected ?? false;

        public void Dispose()
        {
            if (Connected)
            {
                CloseConnection();
            }
            _streamReader.Dispose();
            _streamWriter.Dispose();
            _tcpClient.Dispose();
        }

        public async ValueTask DisposeAsync()
        {
            if (Connected)
            {
                await CloseConnectionAsync();
            }
            _streamReader.Dispose();
            await _streamWriter.DisposeAsync();
            _tcpClient.Dispose();
        }
    }
}