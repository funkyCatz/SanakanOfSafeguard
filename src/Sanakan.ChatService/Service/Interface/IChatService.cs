﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace Sanakan.ChatService.Service.Interface
{
    public interface IChatService : IDisposable, IAsyncDisposable
    {
        /// <summary>
        /// Connect to the Chat Service. For tech details look at https://dev.twitch.tv/docs/irc/membership/
        /// </summary>
        Task StartAsync(CancellationToken stoppingToken = default);

        /// <summary>
        /// Disconnect 
        /// </summary>
        /// <returns>void</returns>
        Task StopAsync(CancellationToken stoppingToken = default);

        /// <summary>
        /// Adds message to the inner message queue
        /// </summary>
        /// <param name="msg">message</param>
        void AddMessage(string msg);

        /// <summary>
        /// Adds message to the inner message queue
        /// </summary>
        /// <param name="msg">message</param>
        /// <param name="stoppingToken">cancellation token</param>
        /// <returns>task</returns>
        Task AddMessageAsync(string msg, CancellationToken stoppingToken = default);

        /// <summary>
        /// Connection flag
        /// </summary>
        bool Connected { get; }
    }
}