﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace Sanakan.ChatService.Service.Interface
{
    public interface IChatClient : IDisposable, IAsyncDisposable
    {
        Task ConnectAsync(string serverUrl, bool useSsl);
        void Connect(string serverUrl, bool useSsl);

        Task<string> ReadMessageAsync();
        Task SendMessageAsync(string message, CancellationToken stoppingToken = default);

        Task CloseConnectionAsync(CancellationToken stoppingToken = default);
        void CloseConnection();
        
        bool Connected { get; }
    }
}