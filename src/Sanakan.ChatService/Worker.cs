using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Sanakan.ChatService.Service.Interface;
using Sanakan.TwitchService.Service.Interfaces;

namespace Sanakan.ChatService
{
    public class Worker : BackgroundService
    {
        private readonly ILogger<Worker> _logger;
        private readonly IChatService _chatService;
        private readonly ITwitchChatService _twitchChatService;

        public Worker(ILogger<Worker> logger,
            IChatService chatService,
            ITwitchChatService twitchChatService)
        {
            _logger = logger;
            _chatService = chatService;
            _twitchChatService = twitchChatService;
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            try
            {
                _logger.LogInformation("Worker running at: {time}", DateTimeOffset.UtcNow);
                
                await _chatService.StartAsync(stoppingToken);
                await _twitchChatService.StartAsync(stoppingToken);
            
                while (!stoppingToken.IsCancellationRequested)
                {
                    await Task.Delay(1000, stoppingToken);
                }
            }
            catch (Exception e)
            {
                _logger.LogError(e, e.Message);
            }
        }
        
        public override async Task StopAsync(CancellationToken stoppingToken)
        {
            await _chatService.StopAsync(stoppingToken);
            await _twitchChatService.StopAsync(stoppingToken);
            
            await _chatService.DisposeAsync();
            
            _logger.LogInformation("Worker stopped at: {time}", DateTimeOffset.UtcNow);
        }
    }
}