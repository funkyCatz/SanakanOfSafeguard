﻿using System;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.Extensions.Logging;
using Sanakan.ChatService.Service.Interface;
using Sanakan.Common.Models.EventBusModels;

namespace Sanakan.ChatService.NotificationHandlers
{
    public class TwitchCommandHandler : INotificationHandler<TwitchCommand>
    {
        private IChatService _chatService;
        private ILogger<TwitchCommandHandler> _logger;

        public TwitchCommandHandler(IChatService chatService,
            ILogger<TwitchCommandHandler> logger)
        {
            _chatService = chatService;
            _logger = logger;
        }
        
        public async Task Handle(TwitchCommand notification, CancellationToken cancellationToken)
        {
            try
            {
                //_logger.LogInformation(notification.Value);
                await _chatService.AddMessageAsync(notification.Value, cancellationToken);
            }
            catch (Exception e)
            {
                _logger.LogError(e, e.Message);
            }
        }
    }
}