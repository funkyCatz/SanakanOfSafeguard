﻿namespace Sanakan.ChatService.Options
{
    public class SettingsOption
    {
        public string ServerUrl   { get; set; }
        public bool UseSsl        { get; set; }
    }
}