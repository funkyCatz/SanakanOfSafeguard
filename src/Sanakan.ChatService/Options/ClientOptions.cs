﻿namespace Sanakan.ChatService.Options
{
    public class ClientOptions
    {
        public int MsgQueueCapacity { get; set; }
        public int MsgCacheItemTimeout { get; set; }
        public int MsgSendDelay { get; set; }
        public int MsgAllowedInPeriod { get; set; }
        public int ThrottlingPeriod { get; set; }
        public int ReconnectionAttempts { get; set; }
    }
}