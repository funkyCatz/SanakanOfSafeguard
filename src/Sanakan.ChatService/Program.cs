using System;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Serilog;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Sanakan.ChatService.Options;
using Sanakan.ChatService.Service;
using Sanakan.ChatService.Service.Interface;
using Sanakan.Common.Constants;
using MediatR;
using Sanakan.DataAccess;
using Sanakan.DataAccess.Options;
using Sanakan.DataAccess.RavenDB;
using Sanakan.TwitchService;
using Serilog.Events;

namespace Sanakan.ChatService
{
    public class Program
    {
        public static async Task Main(string[] args)
        {
            try
            {
                Log.Logger = new LoggerConfiguration()
                    .MinimumLevel.Debug()
                    .MinimumLevel.Override("Microsoft", LogEventLevel.Information)
                    .Enrich.FromLogContext()
                    .WriteTo.Console(outputTemplate: InfrastructureConsts.SerilogConfig.ConsoleOutputTemplate)
                    .WriteTo.Async(configuration =>
                        {
                            configuration.File(@"Log\Sanakan_ChatService_Log.txt",
                                outputTemplate: InfrastructureConsts.SerilogConfig.FileOutputTemplate);
                        })
                    .CreateLogger();
                    
                await CreateHostBuilder(args).Build().RunAsync();
            }
            catch(Exception ex)
            {
                Log.Error(ex, ex.Message);
            }
            finally
            {
                Log.CloseAndFlush();
            }
        }

        private static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .UseSerilog()
                .ConfigureAppConfiguration( (hostContext, config) =>
                {
                    config.AddJsonFile("appsettings.json", optional: false);
                    config.AddJsonFile($"appsettings.{hostContext.HostingEnvironment.EnvironmentName}.json", optional : true);
                })
                .ConfigureServices((hostContext, services) =>
                {
                    services.Configure<ClientOptions>(hostContext.Configuration.GetSection("Client"));
                    services.Configure<SettingsOption>(hostContext.Configuration.GetSection("Settings"));
                    services.Configure<DataBaseOptions>(hostContext.Configuration.GetSection("DataBase"));
                    
                    services.AddMediatR(typeof(Program), 
                        typeof(Common.Module));
                    
                    services.RegisterDataAccess();
                    services.RegisterRavenDb();
                    services.RegisterTwitchService(hostContext);
                    services.AddSingleton<IChatService, Service.ChatService>();
                    services.AddSingleton<IChatClient, TcpChatClient>();

                    services.AddHostedService<Worker>();
                    
                });
        
    }
}