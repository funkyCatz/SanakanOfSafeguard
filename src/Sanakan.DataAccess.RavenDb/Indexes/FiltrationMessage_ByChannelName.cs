﻿using System.Linq;
using Raven.Client.Documents.Indexes;
using Sanakan.DataAccess.RavenDB.Models;

namespace Sanakan.DataAccess.RavenDB.Indexes
{
    public class FiltrationMessage_ByChannelName : AbstractIndexCreationTask<FiltrationMessages>
    {
        public class Result
        {
            public string Text { get; set; }
            public string Channel { get; set; }
        }

        public FiltrationMessage_ByChannelName()
        {
            Map = messages => from message in messages
                select new Result
                {
                    Text = message.Text,
                    Channel = message.Channel
                };
        }
    }
}