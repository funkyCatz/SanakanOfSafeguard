﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using Raven.Client.Documents;
using Raven.Client.Documents.Linq;
using Sanakan.DataAccess.Domain.Entities;
using Sanakan.DataAccess.Repository.Interfaces;

namespace Sanakan.DataAccess.RavenDB.Repository
{
    [Obsolete]
    public class RevenDbRepository<TEntity> : IAsyncRepository<TEntity> where TEntity : Identity
    {
        private readonly IDocumentStore _store;

        public RevenDbRepository(IDocumentStoreLifecycle documentStoreLifecycle)
        {
            _store = documentStoreLifecycle.Store;
        }
        
        public async Task UpdateAsync(TEntity newObj,
            CancellationToken stoppingToken = default)
        {
            var session = _store.OpenAsyncSession();
            await session.StoreAsync(newObj, stoppingToken);
            await session.SaveChangesAsync(stoppingToken);
        }

        public async Task<TEntity> GetAsync(string Id, 
            CancellationToken stoppingToken = default)
        { 
            var session =  _store.OpenAsyncSession();
            
            return await session.LoadAsync<TEntity>(Id, stoppingToken);
        }

        public async Task<IEnumerable<TEntity>> GetAllAsync(CancellationToken stoppingToken = default)
        {
            var session =  _store.OpenAsyncSession();
            return await session.Query<TEntity>().ToListAsync(stoppingToken);
        }

        public async Task<IEnumerable<TEntity>> FindAsync(Expression<Func<TEntity, bool>> predicate, 
            CancellationToken stoppingToken = default)
        {
            var session =  _store.OpenAsyncSession();
            return await session.Query<TEntity>().Where(predicate).ToListAsync(stoppingToken);
        }

        public Task DeleteAsync(string Id, 
            CancellationToken stoppingToken = default)
        {
            var session =  _store.OpenAsyncSession();
            session.Delete(Id);
            return session.SaveChangesAsync(stoppingToken);
        }
        
        public Task DeleteAsync(TEntity entity, 
            CancellationToken stoppingToken = default)
        {
            var session =  _store.OpenAsyncSession();
            session.Delete(entity);
            return session.SaveChangesAsync(stoppingToken);
        }

        public async Task DeleteAsync(IAsyncEnumerable<string> ids, 
            CancellationToken stoppingToken = default)
        {
            var session =  _store.OpenAsyncSession();
            await foreach (var id in ids.WithCancellation(stoppingToken))
            {
                session.Delete(id);
            }
            await session.SaveChangesAsync(stoppingToken);
        }

        public Task DeleteAsync(IEnumerable<string> ids, 
            CancellationToken stoppingToken = default)
        {
            var session =  _store.OpenAsyncSession();
            foreach (var id in ids)
            {
                session.Delete(id);
            }
            return session.SaveChangesAsync(stoppingToken);
        }
    }
}