﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using Raven.Client.Documents;
using Sanakan.DataAccess.Domain.Entities;
using Sanakan.DataAccess.Services.Interfaces;
using AutoMapper;
using AutoMapper.Extensions.ExpressionMapping;
using Raven.Client.Documents.Linq;
using Sanakan.DataAccess.RavenDB.Models;

namespace Sanakan.DataAccess.RavenDB.Services
{
    public class UserEventLogsService : IUserEventLogsService
    {
        private readonly IDocumentStore _store;
        private readonly IMapper _mapper;

        public UserEventLogsService(IDocumentStoreLifecycle documentStoreLifecycle,
            IMapper mapper)
        {
            _store = documentStoreLifecycle.Store;
            _mapper = mapper;
        }
        
        public async Task AddAsync(UserEventLog log, CancellationToken stoppingToken)
        {
            var model = _mapper.Map<UserEventLogs>(log);

            var session = _store.OpenAsyncSession();
            await session.StoreAsync(model, stoppingToken);
            await session.SaveChangesAsync(stoppingToken);
        }

        public async Task<List<UserEventLog>> GetAsync(CancellationToken stoppingToken)
        {
            var session = _store.OpenAsyncSession();
            var models = await session.Query<UserEventLogs>().ToListAsync(stoppingToken);

            return _mapper.Map<List<UserEventLog>>(models);
        }

        public async Task<List<UserEventLog>> GetAsync(Expression<Func<UserEventLog, bool>> exp, CancellationToken stoppingToken)
        {
            var searchExpr = _mapper.MapExpression<Expression<Func<UserEventLogs, bool>>>(exp);

            var session = _store.OpenAsyncSession();
            var models = await session.Query<UserEventLogs>().Where(searchExpr).ToListAsync(stoppingToken);
            
            return _mapper.Map<List<UserEventLog>>(models); 
        }
    }
}