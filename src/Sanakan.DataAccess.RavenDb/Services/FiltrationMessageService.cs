﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Primitives;
using Raven.Client.Documents;
using Raven.Client.Documents.Linq;
using Sanakan.DataAccess.Cache.Interfaces;
using Sanakan.DataAccess.Domain.Entities;
using Sanakan.DataAccess.RavenDB.Indexes;
using Sanakan.DataAccess.RavenDB.Models;
using Sanakan.DataAccess.Services.Interfaces;

namespace Sanakan.DataAccess.RavenDB.Services
{
    public class FiltrationMessageService : IFiltrationMessageService
    {
        private readonly ISignal _signal;
        private readonly IDocumentStore _store;
        private readonly IMemoryCache _memoryCache;
        private readonly IMapper _mapper;

        public FiltrationMessageService(ISignal signal, 
            IDocumentStoreLifecycle documentStoreLifecycle, 
            IMemoryCache memoryCache,
            IMapper mapper)
        {
            _signal = signal;
            _store = documentStoreLifecycle.Store;
            _memoryCache = memoryCache;
            _mapper = mapper;
        }
        
        private IChangeToken ChangeToken => _signal.GetToken(nameof(FiltrationMessage));
        
        public async Task AddAsync(FiltrationMessage message, CancellationToken stoppingToken)
        {
            var storedEntities = await GetAsync(message.Channel, stoppingToken);

            if (!storedEntities.Any( x => string.Equals(x.Text, message.Text)))
            {
                var session = _store.OpenAsyncSession();
                await session.StoreAsync(_mapper.Map<FiltrationMessages>(message), stoppingToken);
                await session.SaveChangesAsync(stoppingToken);
            
                _signal.SignalToken(nameof(FiltrationMessage));
            }
        }

        public async Task<List<FiltrationMessage>> GetAsync(CancellationToken stoppingToken)
        {
            return await _memoryCache.GetOrCreateAsync(nameof(FiltrationMessage), async entry =>
            {
                var session = _store.OpenAsyncSession();
                var entities = await session.Query<FiltrationMessages>().ToListAsync(stoppingToken);
                
                entry.ExpirationTokens.Add(ChangeToken);

                return _mapper.Map<List<FiltrationMessage>>(entities);
            });
        }

        public async Task<List<FiltrationMessage>> GetAsync(string channelName, CancellationToken stoppingToken)
        {
            return await _memoryCache.GetOrCreateAsync(nameof(FiltrationMessage), async entry =>
            {
                var session = _store.OpenAsyncSession();
                var entities = await session.Query<FiltrationMessage_ByChannelName.Result, FiltrationMessage_ByChannelName>()
                    .Where(entity => entity.Channel == channelName)
                    .OfType<FiltrationMessages>()
                    .ToListAsync(stoppingToken);
                
                entry.ExpirationTokens.Add(ChangeToken);
                return _mapper.Map<List<FiltrationMessage>>(entities);
            });
        }
        
        public async Task DeleteAsync(string message, string channel, CancellationToken stoppingToken)
        {
            var session = _store.OpenAsyncSession();
            var entities = await session.Query<FiltrationMessage_ByChannelName.Result, FiltrationMessage_ByChannelName>()
                .Where(x => x.Channel == channel && x.Text == message)
                .OfType<FiltrationMessages>()
                .ToListAsync(stoppingToken);

            foreach (var messageModel in entities)
            {
                session.Delete(messageModel);
            }
            
            _signal.SignalToken(nameof(FiltrationMessage));

            await session.SaveChangesAsync(stoppingToken);
        }
    }
}