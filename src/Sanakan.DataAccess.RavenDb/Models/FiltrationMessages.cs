﻿using System;
using Sanakan.DataAccess.Domain.Entities;
using Sanakan.DataAccess.Mapping.Interfaces;

namespace Sanakan.DataAccess.RavenDB.Models
{
    public class FiltrationMessages : IMapFrom<FiltrationMessage>
    {
        public string Id { get; set; }
        public int UserId { get; set; }
        public string Author { get; set; }
        public string Text { get; set; }
        public DateTime TimeStamp { get; set; }
        public string Channel { get; set; }
    }
}