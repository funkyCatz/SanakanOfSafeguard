﻿using System;
using AutoMapper;
using Sanakan.DataAccess.Domain.Entities;
using Sanakan.DataAccess.Mapping.Interfaces;

namespace Sanakan.DataAccess.RavenDB.Models
{
    public class UserEventLogs : IMapFrom<UserEventLog>
    {
        public string Id { get; set; }

        public string UserName { get; set; }

        public UserEventModelEnum Event { get; set; }

        public string Channel { get; set; }

        public DateTime TimeStamp { get; set; }

        public int UserId { get; set; }

        public string RawInfo { get; set; }
        
        public void Mapping(Profile profile)
        {
            profile.CreateMap<UserEventLog, UserEventLogs>()
                .ForMember(d => d.Id, 
                    opt => opt.Ignore())
                .ReverseMap();
        }
    }

    public enum UserEventModelEnum
    {
        Info,
        Timeout,
        Ban,
        Unban,
        Join,
        Left,
    }
}