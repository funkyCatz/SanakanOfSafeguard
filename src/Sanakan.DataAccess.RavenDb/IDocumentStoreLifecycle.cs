﻿using Raven.Client.Documents;

namespace Sanakan.DataAccess.RavenDB
{
    public interface IDocumentStoreLifecycle
    {
        public IDocumentStore Store { get; }
    }
}