﻿using System;
using System.Reflection;
using AutoMapper;
using Microsoft.Extensions.DependencyInjection;
using Sanakan.DataAccess.RavenDB.Services;
using Sanakan.DataAccess.Services.Interfaces;

namespace Sanakan.DataAccess.RavenDB
{
    public static class DependencyInjection
    {
        public static void RegisterRavenDb(this IServiceCollection services)
        {
            try
            {
                services.AddSingleton<IDocumentStoreLifecycle, DocumentStoreLifecycle>();
                services.AddAutoMapper(Assembly.GetExecutingAssembly());

                services.AddSingleton<IUserEventLogsService, UserEventLogsService>();
                services.AddSingleton<IFiltrationMessageService, FiltrationMessageService>();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }
    }
}