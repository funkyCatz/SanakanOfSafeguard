﻿using System;
using System.Security.Cryptography.X509Certificates;
using Microsoft.Extensions.Options;
using Raven.Client.Documents;
using Raven.Client.Documents.Operations;
using Raven.Client.Exceptions;
using Raven.Client.Exceptions.Database;
using Raven.Client.ServerWide;
using Raven.Client.ServerWide.Operations;
using Sanakan.DataAccess.Options;
using Sanakan.DataAccess.RavenDB.Indexes;

namespace Sanakan.DataAccess.RavenDB
{
    // Should be registered as Singleton
    public class DocumentStoreLifecycle : IDisposable, IDocumentStoreLifecycle
    {
        public IDocumentStore Store { get; }

        public DocumentStoreLifecycle(IOptions<DataBaseOptions> options)
        {
            var store = new DocumentStore
            {
                Urls = new[] { options.Value.DatabaseUrl }, 
                Database = options.Value.DatabaseName
            };

            if (!string.IsNullOrWhiteSpace(options.Value.CertPath))
            {
                store.Certificate = new X509Certificate2(options.Value.CertPath);
            }
            
            Store = store.Initialize();

            CreateDataBaseIfNotExists(options.Value.DatabaseName);

            CreateIndexes();
        }

        private void CreateDataBaseIfNotExists(string database = null)
        {
            database ??= Store.Database;

            if (string.IsNullOrWhiteSpace(database))
            {
                 throw new ArgumentException("Value cannot be null or empty\\whitespace.", nameof(database));
            }

            try
            {
                Store.Maintenance.ForDatabase(database).Send(new GetStatisticsOperation());
            }
            catch (DatabaseDoesNotExistException)
            {
                try
                {
                    Store.Maintenance.Server.Send(new CreateDatabaseOperation(new DatabaseRecord(database)));
                }
                catch (ConcurrencyException) // The database was already created before calling CreateDatabaseOperation
                { }
            }
        }

        private void CreateIndexes()
        {
            new FiltrationMessage_ByChannelName().Execute(Store);
        }
        
        public void Dispose() =>  Store.Dispose();
    }
}