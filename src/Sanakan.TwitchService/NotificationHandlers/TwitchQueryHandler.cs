﻿using System;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.Extensions.Logging;
using Sanakan.Common.Models.EventBusModels;
using Sanakan.TwitchService.Service.Interfaces;

namespace Sanakan.TwitchService.NotificationHandlers
{
    public class  TwitchQueryHandler : INotificationHandler<TwitchQuery>
    {
        private readonly ILogger<TwitchQueryHandler> _logger;
        private readonly ITwitchChatService _twitchChatService;

        public TwitchQueryHandler(ILogger<TwitchQueryHandler> logger,
            ITwitchChatService twitchChatService)
        {
            _logger = logger;
            _twitchChatService = twitchChatService;
        }

        public async Task Handle(TwitchQuery notification, CancellationToken cancellationToken)
        {
            try
            {
                await _twitchChatService.HandleMessageAsync(notification.Value, cancellationToken);
            }
            catch (Exception e)
            {
                _logger.LogError(e, e.Message);
            }
        }
    }
}