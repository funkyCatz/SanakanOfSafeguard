using System.Reflection;
using MediatR;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Sanakan.Common.MediatR;
using Sanakan.Common.QueryDispatcher;
using Sanakan.TwitchService.CommandHandler;
using Sanakan.TwitchService.CommandHandler.Commands.Interfaces;
using Sanakan.TwitchService.CommandHandler.Interfaces;
using Sanakan.TwitchService.Filtration;
using Sanakan.TwitchService.Filtration.Interfaces;
using Sanakan.TwitchService.Options;
using Sanakan.TwitchService.Service;
using Sanakan.TwitchService.Service.Interfaces;
using Sanakan.TwitchService.Service.IrcMessageParser;
using Sanakan.TwitchService.Service.IrcMessageParser.Interfaces;
using Sanakan.TwitchService.Service.JoinedChannelsManager;
using Sanakan.TwitchService.TwitchService.JoinedChannelsManager.Interfaces;

namespace Sanakan.TwitchService
{
    public static class DependencyInjection
    {
        public static void RegisterTwitchService(this IServiceCollection services, HostBuilderContext hostContext)
        {
            services.Configure<CommandOptions>(hostContext.Configuration.GetSection("Commands"));
            services.Configure<LoginOptions>(hostContext.Configuration.GetSection("Login"));
            services.AddMediatR(Assembly.GetExecutingAssembly());
            
            services.RegisterAllTypes<IIrcMessage>(new []{Assembly.GetExecutingAssembly()});
            services.RegisterAllTypes<ITwitchCommand>(new []{Assembly.GetExecutingAssembly()});        
            services.AddSingleton<ITwitchChatService, TwitchChatService>();
                    
            services.AddTransient<IJoinedChannelsManager, JoinedChannelsManager>();
            services.AddTransient<IIrcMessageParser, IrcMessageParser>();
            services.AddTransient<IQueryDispatcher, MSDIQueryDispatcher>();
            services.AddTransient<IJoinedChannelsManager, JoinedChannelsManager>();
            services.AddTransient<IIrcCommandMessageHandler, IrcMessagesHandler>();
            services.AddTransient<IFilter, AntiSpamFiler>();
            services.AddTransient<ITwitchCommandsHandler, TwitchCommandsHandler>();
        }
    }
}