﻿namespace Sanakan.TwitchService
{
    public static class Constants
    {
        public static class LoginStepConstants
        {
            public static string MembershipStep = "CAP REQ twitch.tv/membership";
            public static string CommandsStep = "CAP REQ twitch.tv/commands";
            public static string TagsStep = "CAP REQ twitch.tv/tags";
        }
    }
}