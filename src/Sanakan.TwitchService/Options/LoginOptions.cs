﻿namespace Sanakan.TwitchService.Options
{
    public class LoginOptions
    {
        public string UserName { get; set; }
        public string ChannelName { get; set; }
        public string TwitchToken { get; set; }
    }
}