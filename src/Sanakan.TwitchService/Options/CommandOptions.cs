﻿namespace Sanakan.TwitchService.Options
{
    public class CommandOptions
    {
        public string CommandTokens { get; set; }
    }
}