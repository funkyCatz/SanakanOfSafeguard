﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Sanakan.Common.Models.TwitchModels;
using Sanakan.DataAccess.Services.Interfaces;
using Sanakan.TwitchService.Filtration.Interfaces;

namespace Sanakan.TwitchService.Filtration
{
    public class AntiSpamFiler : IFilter
    {
        private IFiltrationMessageService _filtrationMessageService;

        public AntiSpamFiler(IFiltrationMessageService filtrationMessageService)
        {
            _filtrationMessageService = filtrationMessageService;

            Name = nameof(AntiSpamFiler);
        }
        
        public async Task<FiltrationActionEnum> FilterAsync(IrcMessage message, CancellationToken stoppingToken = default)
        {
            if (!string.IsNullOrWhiteSpace(message.Message))
            {
                var records = await _filtrationMessageService.GetAsync(message.Channel, stoppingToken);
                if (records.Any(filtrationRecord => message.Message.Contains(filtrationRecord.Text)))
                {
                    return FiltrationActionEnum.DeleteMsg;
                }
            }

            return FiltrationActionEnum.Nothing;
        }

        public FiltrationActionEnum Filter(IrcMessage message)
        {
            var task = Task.Run(async () => await FilterAsync(message));
            return task.Result;
        }

        public string Name { get; }
    }
}