﻿using System.Threading;
using System.Threading.Tasks;
using Sanakan.Common.Models.TwitchModels;

namespace Sanakan.TwitchService.Filtration.Interfaces
{
    public interface IFilter
    {
        Task<FiltrationActionEnum> FilterAsync(IrcMessage message, CancellationToken stoppingToken = default);
        FiltrationActionEnum Filter(IrcMessage message);
        
        string Name { get; }
    }
}