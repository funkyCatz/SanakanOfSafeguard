﻿using System.Collections.Concurrent;
using Sanakan.Common.Models.TwitchModels;

namespace Sanakan.TwitchService.TwitchService.JoinedChannelsManager.Interfaces
{
    public interface IJoinedChannelsManager
    {
        void Reset();
        JoinedChannel GetChannel(string channelName);
        void AddChannel(JoinedChannel channel);
        void RemoveChannel(string channelName);
        ConcurrentDictionary<string, JoinedChannel> Channels { get; }
    }
}