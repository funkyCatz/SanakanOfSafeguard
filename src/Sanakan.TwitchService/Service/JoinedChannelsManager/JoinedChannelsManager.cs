﻿using System.Collections.Concurrent;
using Sanakan.Common.Models.TwitchModels;
using Sanakan.TwitchService.TwitchService.JoinedChannelsManager.Interfaces;

namespace Sanakan.TwitchService.Service.JoinedChannelsManager
{
    public class JoinedChannelsManager : IJoinedChannelsManager
    {
        public JoinedChannelsManager()
        {
            Channels = new ConcurrentDictionary<string, JoinedChannel>();
        }

        public void Reset() => Channels.Clear();

        public JoinedChannel GetChannel(string channelName)
        {
            Channels.TryGetValue(channelName, out var joinedChannel);
            return joinedChannel;
        }

        public void AddChannel(JoinedChannel channel) => Channels.TryAdd(channel.Channel, channel);

        public void RemoveChannel(string channelName) => Channels.TryRemove(channelName, out _);
        public ConcurrentDictionary<string, JoinedChannel> Channels { get; }
    }
}