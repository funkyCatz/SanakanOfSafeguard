﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Sanakan.Common.Models.TwitchModels;
using Sanakan.TwitchService.Service.Interfaces;

namespace Sanakan.TwitchService.Service
{
    public class IrcMessagesHandler : IIrcCommandMessageHandler
    {
        public IDictionary<IrcCommandEnum, IIrcMessage> _commandMessageHandlerDict;
        private ILogger<IrcMessagesHandler> _logger;

        public IrcMessagesHandler(IEnumerable<IIrcMessage> commandMessages,
            ILogger<IrcMessagesHandler> logger)
        {
            _logger = logger;
            _commandMessageHandlerDict = commandMessages.ToDictionary(x => x.Command);
        }
        
        
        public void Handle(IrcMessage ircMessage)
        {
            var task = Task.Run(async () => await HandleAsync(ircMessage));
            task.Wait();
        }

        public async Task HandleAsync(IrcMessage ircMessage)
        {
            if (_commandMessageHandlerDict.TryGetValue(ircMessage.Command, out var commandHandler))
            {
                await commandHandler.HandleAsync(ircMessage);
            }
            else
            {
                _logger.LogInformation($"Command handler for IRC command {ircMessage.Command} was not found.");
            }
        }
    }
}