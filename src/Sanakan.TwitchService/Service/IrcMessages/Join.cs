﻿using System;
using System.Threading.Tasks;
using MediatR;
using Sanakan.Common.Models.TwitchModels;
using Sanakan.DataAccess.Commands;
using Sanakan.DataAccess.Domain.Entities;
using Sanakan.DataAccess.Repository.Interfaces;
using Sanakan.TwitchService.Service.Interfaces;

namespace Sanakan.TwitchService.Service.IrcMessages
{
    public class Join : IIrcMessage
    {
        public IrcCommandEnum Command { get; }

        private readonly IMediator _mediator;

        public Join(IMediator mediator)
        {
            Command = IrcCommandEnum.Join;

            _mediator = mediator;
        }
        
        public void Handle(IrcMessage ircMessage)
        {
            var task = Task.Run(async () => await HandleAsync(ircMessage));
            task.Wait();
        }

        public async Task HandleAsync(IrcMessage ircMessage)
        {
            await _mediator.Send(new AddUserVisitCommand
            {
                UserVisit = new UserEventLog
                {
                    Channel = ircMessage.Channel,
                    TimeStamp = DateTime.UtcNow,
                    UserName = ircMessage.User,
                    Event = UserEventEnum.Join,
                    RawInfo = ircMessage.ToString()
                }
            });
        }
    }
}