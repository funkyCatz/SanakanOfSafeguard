﻿using System;
using System.Threading.Tasks;
using MediatR;
using Sanakan.Common.Models.TwitchModels;
using Sanakan.DataAccess.Commands;
using Sanakan.DataAccess.Domain.Entities;
using Sanakan.TwitchService.Service.Interfaces;

namespace Sanakan.TwitchService.Service.IrcMessages
{
    public class Part : IIrcMessage
    {
        public IrcCommandEnum Command { get; }
        
        private readonly IMediator _mediator;

        public Part(IMediator mediator)
        {
            Command = IrcCommandEnum.Part;

            _mediator = mediator;
        }
        
        public void Handle(IrcMessage ircMessage)
        {
            var task = Task.Run(async () => await HandleAsync(ircMessage));
            task.Wait();
        }

        public async Task HandleAsync(IrcMessage ircMessage)
        {
            await _mediator.Send( new AddUserVisitCommand
            {
                UserVisit = new UserEventLog
                {
                    Channel = ircMessage.Channel,
                    Event = UserEventEnum.Left,
                    TimeStamp = DateTime.UtcNow,
                    UserName = ircMessage.User,
                    RawInfo = ircMessage.ToString()
                }
            });
        }
    }
}