﻿using System.Threading.Tasks;
using MediatR;
using Sanakan.Common.Models.EventBusModels;
using Sanakan.Common.Models.TwitchModels;
using Sanakan.TwitchService.Service.Interfaces;

namespace Sanakan.TwitchService.Service.IrcMessages
{
    public class Ping : IIrcMessage
    {
        public IrcCommandEnum Command { get; }

        private const string PongReply = "PONG :tmi.twitch.tv";
        
        private IMediator _mediatr;

        public Ping(IMediator mediatr)
        {
            _mediatr = mediatr;
            Command = IrcCommandEnum.Ping;
        }
        
        public void Handle(IrcMessage ircMessage)
        {
            var task = Task.Run(async () => await HandleAsync(ircMessage));
            task.Wait();
        }

        public async Task HandleAsync(IrcMessage ircMessage)
        {
            await _mediatr.Publish(new TwitchCommand(PongReply));
        }
    }
}