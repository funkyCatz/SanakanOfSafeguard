﻿using System.Collections.Generic;
using System.Threading.Tasks;
using MediatR;
using Microsoft.Extensions.Options;
using Sanakan.Common.Models.EventBusModels;
using Sanakan.Common.Models.TwitchModels;
using Sanakan.TwitchService.CommandHandler.Interfaces;
using Sanakan.TwitchService.Filtration;
using Sanakan.TwitchService.Filtration.Interfaces;
using Sanakan.TwitchService.Options;
using Sanakan.TwitchService.Service.Interfaces;

namespace Sanakan.TwitchService.Service.IrcMessages
{
    public class PrivMsg : IIrcMessage
    {
        public IrcCommandEnum Command { get; }
        private readonly IEnumerable<IFilter> _filters;
        private readonly IMediator _mediator;
        private readonly ITwitchCommandsHandler _twitchCommandsHandler;
        private string UserName { get; }
        private MessageEmoteCollection _channelEmotes;
        
        public PrivMsg(IMediator mediator, IEnumerable<IFilter> filters, 
            ITwitchCommandsHandler twitchCommandsHandler, IOptions<LoginOptions> loginOptions)
        {
            Command = IrcCommandEnum.PrivMsg;
            _mediator = mediator;
            _twitchCommandsHandler = twitchCommandsHandler;
            _filters = filters;
            UserName = loginOptions.Value.UserName;
            _channelEmotes = new MessageEmoteCollection();
        }
        
        public void Handle(IrcMessage ircMessage)
        {
            var task = Task.Run(async () => await HandleAsync(ircMessage));
            task.Wait();
        }

        public async Task HandleAsync(IrcMessage ircMessage)
        {
            foreach (var filter in _filters)
            {
                var filtrationResult = await filter.FilterAsync(ircMessage);

                switch (filtrationResult)
                {
                    case FiltrationActionEnum.BanAuthor:
                        await _mediator.Publish(new TwitchCommand($"/ban {ircMessage.User}"));
                        return;
                    case FiltrationActionEnum.DeleteMsg:
                        await _mediator.Publish(new TwitchCommand($"/timeout {ircMessage.User} spamming"));
                        return;
                }
            }
            
            ChatMessage chatMessage = new ChatMessage(UserName, ircMessage, ref _channelEmotes);

            await _twitchCommandsHandler.ExecuteAsync(chatMessage);
        }
    }
}