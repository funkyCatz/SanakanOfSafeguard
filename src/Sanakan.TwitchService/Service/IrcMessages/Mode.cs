﻿using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Sanakan.Common.Models.TwitchModels;
using Sanakan.TwitchService.Service.Interfaces;

namespace Sanakan.TwitchService.Service.IrcMessages
{
    public class Mode : IIrcMessage
    {
        public IrcCommandEnum Command { get; }
        private ILogger<Mode> _logger;

        public Mode(ILogger<Mode> logger)
        {
            Command = IrcCommandEnum.Mode;
            _logger = logger;
        }
        
        public void Handle(IrcMessage ircMessage)
        {
            if (ircMessage.Message.StartsWith("+o"))
            {
                // TODO: Save that info in to the RavenDB
                _logger.LogInformation($"Moderator {ircMessage.Message.Split(' ')[1]} joined channel {ircMessage.Channel}");
            }
            else if (ircMessage.Message.StartsWith("-o"))
            {
                _logger.LogInformation($"Moderator {ircMessage.Message.Split(' ')[1]} left channel {ircMessage.Channel}");
            }
        }

        public async Task HandleAsync(IrcMessage ircMessage)
        {
            await Task.Run(() => Handle(ircMessage));
        }
    }
}