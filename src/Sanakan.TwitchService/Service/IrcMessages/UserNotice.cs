﻿using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Sanakan.Common.Models.TwitchModels;
using Sanakan.TwitchService.Service.Interfaces;

namespace Sanakan.TwitchService.Service.IrcMessages
{
    public class UserNotice : IIrcMessage
    {
        public IrcCommandEnum Command { get; }

        private ILogger<UserNotice> _logger;

        public UserNotice(ILogger<UserNotice> logger)
        {
            Command = IrcCommandEnum.UserNotice;
            _logger = logger;
        }

        public void Handle(IrcMessage ircMessage)
        {
            var successMsgId = ircMessage.Tags.TryGetValue(Tags.MsgId, out var msgId);
            if (!successMsgId)
            {
                _logger.LogInformation($"Unaccounted for: {ircMessage.ToString()}");
                return;
            }
/*
            switch (msgId)
            {
                case TwitchMessageIdsConsts.MessageIds.Raid:
                    var raidNotification = new RaidNotification(ircMessage);
                break;
                case TwitchMessageIdsConsts.MessageIds.ReSubscription:
                    var resubscriber = new ReSubscriber(ircMessage); 
                break;
                case TwitchMessageIdsConsts.MessageIds.Ritual:
                    var successRitualName = ircMessage.Tags.TryGetValue(Tags.MsgParamRitualName, out var ritualName);
                    if (!successRitualName)
                    {
                        return;
                    }
                    switch (ritualName)
                    {
                        case "new_chatter":
                            var ritualNewChatter = new RitualNewChatter(ircMessage); 
                        break;
                        default:
                            break;
                    }
                break;
                case TwitchMessageIdsConsts.MessageIds.SubGift:
                    var giftedSubscription = new GiftedSubscription(ircMessage);

                break;
                case TwitchMessageIdsConsts.MessageIds.CommunitySubscription:
                    var communitySubscription = new CommunitySubscription(ircMessage);

                break;
                case TwitchMessageIdsConsts.MessageIds.Subscription:
                    var subscriber = new Subscriber(ircMessage);

                break;
                default:
                    break;
            }
            */
        }

        public async Task HandleAsync(IrcMessage ircMessage)
        {
            await Task.Run(() => Handle(ircMessage));
        }
    }
}