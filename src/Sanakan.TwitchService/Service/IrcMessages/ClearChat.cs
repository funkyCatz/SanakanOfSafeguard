﻿using System;
using System.Threading.Tasks;
using MediatR;
using Sanakan.Common.Models.TwitchModels;
using Sanakan.DataAccess.Commands;
using Sanakan.DataAccess.Domain.Entities;
using Sanakan.TwitchService.Service.Interfaces;

namespace Sanakan.TwitchService.Service.IrcMessages
{
    public class ClearChat : IIrcMessage
    {
        public IrcCommandEnum Command { get; }

        private readonly IMediator _mediator;

        public ClearChat(IMediator mediator)
        {
            Command = IrcCommandEnum.ClearChat;
            _mediator = mediator;
        }
        
        public void Handle(IrcMessage ircMessage)
        {
            var task = Task.Run(async () => await HandleAsync(ircMessage));
            task.Wait();
        }

        public async Task HandleAsync(IrcMessage ircMessage)
        {
            if (!string.IsNullOrWhiteSpace(ircMessage.Message))
            {
                var isNotPermaban = ircMessage.Tags.TryGetValue(Tags.BanDuration, out var banDuration);
                if (isNotPermaban)
                {
                    await _mediator.Send(new AddUserPunishmentCommand
                    {
                        UserPunishment = new UserEventLog
                        {
                            UserName = ircMessage.User,
                            Channel = ircMessage.Channel,
                            Event = UserEventEnum.Timeout,
                            TimeStamp = DateTime.UtcNow,
                            RawInfo = ircMessage.ToString(),
                        }

                    });
                }
                else
                {
                    await _mediator.Send(new AddUserPunishmentCommand
                    {
                        UserPunishment = new UserEventLog
                        {
                            UserName = ircMessage.User,
                            Channel = ircMessage.Channel,
                            Event = UserEventEnum.Ban,
                            TimeStamp = DateTime.UtcNow,
                            RawInfo = ircMessage.ToString(),
                        }
                    });
                }
            }
        }
    }
}