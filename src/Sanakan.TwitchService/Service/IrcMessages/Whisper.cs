﻿using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Sanakan.Common.Models.TwitchModels;
using Sanakan.TwitchService.Service.Interfaces;

namespace Sanakan.TwitchService.Service.IrcMessages
{
    public class Whisper : IIrcMessage
    {
        public IrcCommandEnum Command { get; }
        private ILogger<Whisper> _logger;

        public Whisper(ILogger<Whisper> logger)
        {
            _logger = logger;
            Command = IrcCommandEnum.Whisper;
        }
        
        public void Handle(IrcMessage ircMessage)
        {
            // TODO : log this info in to the RavenDB
            _logger.LogInformation($"Whisper : {ircMessage.Message}");
        }

        public async Task HandleAsync(IrcMessage ircMessage)
        {
            await Task.Run(() => Handle(ircMessage));
        }
    }
}