﻿using System;

namespace Sanakan.TwitchService.Service.Exceptions
{
    public class ClientNotInitializedException : Exception
    {
        public ClientNotInitializedException(string details)
            : base(details)
        {
            
        }
    }
}