﻿using System;

namespace Sanakan.TwitchService.Service.Exceptions
{
    public class InvalidParameterException : Exception
    {
        public string UserName { get; }

        public InvalidParameterException(string userName)
        {
            UserName = userName;
        }
    }
}