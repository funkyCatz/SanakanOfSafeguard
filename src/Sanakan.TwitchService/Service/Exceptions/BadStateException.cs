﻿using System;

namespace Sanakan.TwitchService.Service.Exceptions
{
    public class BadStateException : Exception
    {
        public BadStateException(string details)
            : base(details)
        {
            
        }
    }
}