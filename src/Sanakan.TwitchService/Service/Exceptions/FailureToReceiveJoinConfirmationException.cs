﻿using System;

namespace Sanakan.TwitchService.Service.Exceptions
{
    public class FailureToReceiveJoinConfirmationException : Exception
    {
        public string Channel { get; }

        public FailureToReceiveJoinConfirmationException(string channel)
        {
            Channel = channel;
        }
    }
}