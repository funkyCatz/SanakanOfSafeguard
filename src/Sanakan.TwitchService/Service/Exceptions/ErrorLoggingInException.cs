﻿using System;

namespace Sanakan.TwitchService.Service.Exceptions
{
    public class ErrorLoggingInException : Exception
    {
        public string UserName { get; }

        public ErrorLoggingInException(string details, string userName)
            : base(details)
        {
            UserName = userName;
        }
    }
}