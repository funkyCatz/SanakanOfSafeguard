﻿using System;

namespace Sanakan.TwitchService.Service.Exceptions
{
    public class ClientNotConnectedException : Exception
    {
        public ClientNotConnectedException(string details) 
            : base(details)
        {
            
        }
    }
}