﻿namespace Sanakan.TwitchService.Service.Enums
{
    public enum SendReceiveDirectionEnum
    {
        Sent,
        Received
    }
}