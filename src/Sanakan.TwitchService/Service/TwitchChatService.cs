﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Sanakan.Common.Models.EventBusModels;
using Sanakan.Common.Models.TwitchModels;
using Sanakan.TwitchService.Options;
using Sanakan.TwitchService.Service.Interfaces;
using Sanakan.TwitchService.Service.IrcMessageParser.Helpers;
using Sanakan.TwitchService.Service.IrcMessageParser.Interfaces;
using Sanakan.TwitchService.TwitchService.JoinedChannelsManager.Interfaces;

namespace Sanakan.TwitchService.Service
{
    public class TwitchChatService : ITwitchChatService
    {
        private readonly ILogger<TwitchChatService> _logger;
        private readonly IJoinedChannelsManager _joinedChannelsManager;
        private readonly LoginOptions _loginOptions;
        private readonly IIrcMessageParser _ircMessageParser;
        private readonly IIrcCommandMessageHandler _ircCommandMessageHandler;
        private readonly IMediator _mediator;
        
        private bool Connected { get; set; }

        public TwitchChatService(ILogger<TwitchChatService> logger,
            IJoinedChannelsManager joinedChannelsManager,
            IOptions<LoginOptions> loginOptions,
            IIrcMessageParser ircMessageParser,
            IIrcCommandMessageHandler ircCommandMessageHandler, 
            IMediator mediator)
        {
            _logger = logger;
            _joinedChannelsManager = joinedChannelsManager;
            _ircMessageParser = ircMessageParser;
            _mediator = mediator;
            _loginOptions = loginOptions.Value;

            _ircCommandMessageHandler = ircCommandMessageHandler;
        }
        
        public void HandleMessage(string rawMessage)
        {
            var parsedMessage = _ircMessageParser.ParseIrcMessage(rawMessage);
            _ircCommandMessageHandler.Handle(parsedMessage);
        }

        public async Task HandleMessageAsync(string rawMessage, CancellationToken stoppingToken)
        {
            var parsedMessage = await _ircMessageParser.ParseIrcMessageAsync(rawMessage, stoppingToken);
            await _ircCommandMessageHandler.HandleAsync(parsedMessage);
        }

        public async Task StartAsync(CancellationToken stoppingToken)
        {
            try
            {
                await _mediator.Publish(new TwitchCommand(Rfc2812.Pass(_loginOptions.TwitchToken)), stoppingToken);
                await _mediator.Publish(new TwitchCommand(Rfc2812.Nick(_loginOptions.UserName)), stoppingToken);
                await _mediator.Publish(new TwitchCommand(Rfc2812.User(_loginOptions.UserName, 0, _loginOptions.UserName)),
                    stoppingToken);

                await _mediator.Publish(new TwitchCommand(Constants.LoginStepConstants.MembershipStep), stoppingToken);
                await _mediator.Publish(new TwitchCommand(Constants.LoginStepConstants.CommandsStep), stoppingToken);
                await _mediator.Publish(new TwitchCommand(Constants.LoginStepConstants.TagsStep), stoppingToken);

                await _mediator.Publish(new TwitchCommand(Rfc2812.Join(_loginOptions.ChannelName)), stoppingToken);

                Connected = true;

                if (!String.IsNullOrWhiteSpace(_loginOptions.ChannelName))
                {
                    await JoinChannelAsync(_loginOptions.ChannelName, stoppingToken);
                }
            }
            catch (Exception e)
            {
                _logger.LogError(e, e.Message);
            }
        }
        
        public async Task StopAsync(CancellationToken stoppingToken)
        {
            await Task.Run( () => _joinedChannelsManager.Reset(), stoppingToken);
            Connected = false;
        }
        
        public async Task JoinChannelAsync(string channelName, CancellationToken stoppingToken)
        {
            if (Connected)
            {
                try
                {
                    // If there are no channelName in the JoinedChannelsManager channels
                    if (!_joinedChannelsManager.Channels.Any(x => 
                        String.Equals(x.Key, channelName, StringComparison.InvariantCultureIgnoreCase)))
                    {
                        await _mediator.Publish(new TwitchCommand(Rfc2812.Join(channelName)), stoppingToken);
                        _joinedChannelsManager.AddChannel(new JoinedChannel(channelName));
                    }
                }
                catch (Exception e)
                {
                    _logger.LogError(e, e.Message);
                }
            }
        }

        public async Task LeaveChannelAsync(string channelName, CancellationToken stoppingToken)
        {
            if (Connected)
            {
                try
                {
                    await _mediator.Publish(new TwitchCommand(Rfc2812.Part(channelName)), stoppingToken);
                    _joinedChannelsManager.RemoveChannel(channelName);
                }
                catch (Exception e)
                {
                    _logger.LogError(e, e.Message);
                }
            }
        }
    }
}