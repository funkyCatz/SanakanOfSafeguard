﻿using System;
using System.Text;
using System.Text.RegularExpressions;

namespace Sanakan.TwitchService.Service.IrcMessageParser.Helpers
{
public static class Rfc2812
    {
        // nickname   =  ( letter / special ) *8( letter / digit / special / "-" )
        // letter     =  %x41-5A / %x61-7A       ; A-Z / a-z
        // digit      =  %x30-39                 ; 0-9
        // special    =  %x5B-60 / %x7B-7D
        //                  ; "[", "]", "\", "`", "_", "^", "{", "|", "}"
        private static readonly Regex NicknameRegex = new Regex(@"^[A-Za-z\[\]\\`_^{|}][A-Za-z0-9\[\]\\`_\-^{|}]+$", RegexOptions.Compiled);

        /// <summary>
        /// Checks if the passed nickname is valid according to the RFC
        ///
        /// Use with caution, many IRC servers are not conform with this!
        /// </summary>
        public static bool IsValidNickname(string nickname)
        {
            return !string.IsNullOrEmpty(nickname) &&
                   NicknameRegex.Match(nickname).Success;
        }

        /// <summary>Pass message.</summary>
        public static string Pass(string password) =>  $"PASS {password}";

        /// <summary>Nick message.</summary>
        public static string Nick(string nickname) => $"NICK {nickname}";

        /// <summary>User message.</summary>
        public static string User(string username, int usermode, string realname) => $"USER {username} {usermode} * :{realname}";

        /// <summary>Oper message.</summary>
        public static string Oper(string name, string password) => $"OPER {name} {password}";

        /// <summary>Privmsg message.</summary>
        public static string Privmsg(string destination, string message) =>  $"PRIVMSG {destination} :{message}";

        /// <summary>Notice message.</summary>
        public static string Notice(string destination, string message) => $"NOTICE {destination} :{message}";

        /// <summary>Join message.</summary>
        public static string Join(string channel) => $"JOIN {channel}";

        /// <summary>Join message.</summary>
        public static string Join(string[] channels) => $"JOIN {string.Join(",", channels)}";

        /// <summary>Join message.</summary>
        public static string Join(string channel, string key) => $"JOIN {channel} {key}";

        /// <summary>Join message.</summary>
        public static string Join(string[] channels, string[] keys) => $"JOIN {string.Join(",", channels)} {string.Join(",", keys)}";

        /// <summary>Part message.</summary>
        public static string Part(string channel) => $"PART {channel}";
        

        /// <summary>Part message.</summary>
        public static string Part(string[] channels) => $"PART {string.Join(",", channels)}";

        /// <summary>Part message.</summary>
        public static string Part(string channel, string partmessage) => $"PART {channel} :{partmessage}";

        /// <summary>Part message.</summary>
        public static string Part(string[] channels, string partmessage) => $"PART {string.Join(",", channels)} :{partmessage}";

        /// <summary>Kick message.</summary>
        public static string Kick(string channel, string nickname) => $"KICK {channel} {nickname}";

        /// <summary>Kick message.</summary>
        public static string Kick(string channel, string nickname, string comment) => $"KICK {channel} {nickname} :{comment}";

        /// <summary>Kick message.</summary>
        public static string Kick(string[] channels, string nickname) => $"KICK {string.Join(",", channels)} {nickname}";

        /// <summary>Kick message.</summary>
        public static string Kick(string[] channels, string nickname, string comment) => $"KICK {string.Join(",", channels)} {nickname} :{comment}";

        /// <summary>Kick message.</summary>
        public static string Kick(string channel, string[] nicknames) => $"KICK {channel} {string.Join(",", nicknames)}";

        /// <summary>Kick message.</summary>
        public static string Kick(string channel, string[] nicknames, string comment) => $"KICK {channel} {string.Join(",", nicknames)} :{comment}";

        /// <summary>Kick message.</summary>
        public static string Kick(string[] channels, string[] nicknames) => $"KICK {string.Join(",", channels)} {string.Join(",", nicknames)}";

        /// <summary>Kick message.</summary>
        public static string Kick(string[] channels, string[] nicknames, string comment) => $"KICK {string.Join(",", channels)} {string.Join(",", nicknames)} :{comment}";

        /// <summary>Motd message.</summary>
        public static string Motd() => "MOTD";

        /// <summary>Motd message.</summary>
        public static string Motd(string target) => $"MOTD {target}";

        /// <summary>Luser message.</summary>
        public static string Lusers() => "LUSERS";

        /// <summary>Luser message.</summary>
        public static string Lusers(string mask) => $"LUSER {mask}";

        /// <summary>Lusers</summary>
        public static string Lusers(string mask, string target) => $"LUSER {mask} {target}";

        /// <summary>Version message.</summary>
        public static string Version() => "VERSION";

        /// <summary>Version message.</summary>
        public static string Version(string target) => $"VERSION {target}";

        /// <summary>Stats message.</summary>
        public static string Stats() => "STATS";

        /// <summary>Stats message.</summary>
        public static string Stats(string query) => $"STATS {query}";

        /// <summary>Stats message.</summary>
        public static string Stats(string query, string target) => $"STATS {query} {target}";

        /// <summary>Links message.</summary>
        public static string Links() => "LINKS";

        /// <summary>Links message.</summary>
        public static string Links(string servermask) => $"LINKS {servermask}";

        /// <summary>Links message.</summary>
        public static string Links(string remoteserver, string servermask) => $"LINKS {remoteserver} {servermask}";

        /// <summary>Time message.</summary>
        public static string Time() => "TIME";

        /// <summary>Time message.</summary>
        public static string Time(string target) => $"TIME {target}";

        /// <summary>Connect message.</summary>
        public static string Connect(string targetserver, string port) => $"CONNECT {targetserver} {port}";

        /// <summary>Connect message.</summary>
        public static string Connect(string targetserver, string port, string remoteserver) => $"CONNECT {targetserver} {port} {remoteserver}";

        /// <summary>Trace message.</summary>
        public static string Trace() => "TRACE";

        /// <summary>Trace message.</summary>
        public static string Trace(string target) => $"TRACE {target}";

        /// <summary>Admin message.</summary>
        public static string Admin() => "ADMIN";

        /// <summary>Admin message.</summary>
        public static string Admin(string target) => $"ADMIN {target}";
        
        /// <summary>Info message.</summary>
        public static string Info() => "INFO";

        /// <summary>Info message.</summary>
        public static string Info(string target) => $"INFO {target}";

        /// <summary>Servlist message.</summary>
        public static string Servlist() => "SERVLIST";

        /// <summary>Servlist message.</summary>
        public static string Servlist(string mask) => $"SERVLIST {mask}";

        /// <summary>Servlist message.</summary>
        public static string Servlist(string mask, string type) => $"SERVLIST {mask} {type}";

        /// <summary>Squery message.</summary>
        public static string Squery(string servicename, string servicetext) => $"SQUERY {servicename} :{servicename}";

        /// <summary>List message.</summary>
        public static string List() => "LIST";

        /// <summary>List message.</summary>
        public static string List(string channel) => $"LIST {channel}";

        /// <summary>List message.</summary>
        public static string List(string[] channels) => $"LIST {string.Join(",", channels)}";

        /// <summary>List message.</summary>
        public static string List(string channel, string target) => $"LIST {channel} {target}";

        /// <summary>List message.</summary>
        public static string List(string[] channels, string target) => $"LIST {string.Join(",", channels)} {target}";

        /// <summary>Names message</summary>
        public static string Names() => "NAMES";

        /// <summary>Names message.</summary>
        public static string Names(string channel) => $"NAMES {channel}";

        /// <summary>Names message.</summary>
        public static string Names(string[] channels) => $"NAMES {string.Join(",", channels)}";

        /// <summary>Names message.</summary>
        public static string Names(string channel, string target) => $"NAMES {channel} {target}";

        /// <summary>Names message.</summary>
        public static string Names(string[] channels, string target) => $"NAMES {string.Join(",", channels)} {target}";

        /// <summary>Topic message.</summary>
        public static string Topic(string channel) => $"TOPIC {channel}";

        /// <summary>Topic message.</summary>
        public static string Topic(string channel, string newtopic) => $"TOPIC {channel} :{newtopic}";

        /// <summary>Mode message.</summary>
        public static string Mode(string target) => $"MODE {target}";

        /// <summary>Mode message.</summary>
        public static string Mode(string target, string newmode) => $"MODE {target} {newmode}" + target + " " + newmode;

        /// <summary>Mode message.</summary>
        public static string Mode(string target, string[] newModes, string[] newModeParameters)
        {
            if (newModes == null)
            {
                throw new ArgumentNullException(nameof(newModes));
            }
            if (newModeParameters == null)
            {
                throw new ArgumentNullException(nameof(newModeParameters));
            }
            if (newModes.Length != newModeParameters.Length)
            {
                throw new ArgumentException($"{nameof(newModes)} and {nameof(newModeParameters)} must have the same size.");
            }

            var newMode = new StringBuilder(newModes.Length);
            var newModeParameter = new StringBuilder();
            // as per RFC 3.2.3, maximum is 3 modes changes at once
            const int maxModeChanges = 3;
            if (newModes.Length > maxModeChanges)
            {
                throw new ArgumentOutOfRangeException(
                    nameof(target.Length),
                    newModes.Length,
                    $"Mode change list is too large (> {maxModeChanges})."
                );
            }

            for (var i = 0; i <= newModes.Length; i += maxModeChanges)
            {
                for (var j = 0; j < maxModeChanges; j++)
                {
                    if (i + j >= newModes.Length)
                    {
                        break;
                    }
                    newMode.Append(newModes[i + j]);
                }

                for (var j = 0; j < maxModeChanges; j++)
                {
                    if (i + j >= newModeParameters.Length)
                    {
                        break;
                    }
                    newModeParameter.Append(newModeParameters[i + j]);
                    newModeParameter.Append(" ");
                }
            }

            if (newModeParameter.Length <= 0) return Mode(target, newMode.ToString());

            // remove trailing space
            newModeParameter.Length--;
            newMode.Append(" ");
            newMode.Append(newModeParameter);

            return Mode(target, newMode.ToString());
        }

        /// <summary>Service message.</summary>
        public static string Service(string nickname, string distribution, string info) => $"SERVICE {nickname} * {distribution} * * :{info}";

        /// <summary>Invite message.</summary>
        public static string Invite(string nickname, string channel) => $"INVITE {nickname} {channel}";

        /// <summary>Who message.</summary>
        public static string Who() => "WHO";

        /// <summary>Who message.</summary>
        public static string Who(string mask) => $"WHO {mask}";

        /// <summary>Who message.</summary>
        public static string Who(string mask, bool ircop) => ircop ? $"WHO {mask} o" : $"WHO {mask}";

        /// <summary>Whois message.</summary>
        public static string Whois(string mask) => $"WHOIS {mask}";

        /// <summary>Whois message.</summary>
        public static string Whois(string[] masks) => $"WHOIS {string.Join(",", masks)}";

        /// <summary>Whois message.</summary>
        public static string Whois(string target, string mask) => $"WHOIS {target} {mask}";

        /// <summary>Whois message.</summary>
        public static string Whois(string target, string[] masks) => $"WHOIS {target} {string.Join(",", masks)}";

        /// <summary>Whowas message.</summary>
        public static string Whowas(string nickname) => $"WHOWAS {nickname}";

        /// <summary>Whowas message.</summary>
        public static string Whowas(string[] nicknames) => $"WHOWAS {string.Join(",", nicknames)}";

        /// <summary>Whowas message.</summary>
        public static string Whowas(string nickname, string count) => $"WHOWAS {nickname} {count} ";

        /// <summary>Whowas message.</summary>
        public static string Whowas(string[] nicknames, string count) => $"WHOWAS {string.Join(",", nicknames)} {count} ";

        /// <summary>Whowas message.</summary>
        public static string Whowas(string nickname, string count, string target) => $"WHOWAS {nickname} {count} {target}";

        /// <summary>Whowas message.</summary>
        public static string Whowas(string[] nicknames, string count, string target) => $"WHOWAS {string.Join(",", nicknames)} {count} {target}";

        /// <summary>Kill message.</summary>
        public static string Kill(string nickname, string comment) => $"KILL {nickname} :{comment}";

        /// <summary>Ping message.</summary>
        public static string Ping(string server) => $"PING {server}";

        /// <summary>Ping message.</summary>
        public static string Ping(string server, string server2) => $"PING {server} {server2}";

        /// <summary>Pong message.</summary>
        public static string Pong(string server) => $"PONG {server}";

        /// <summary>Pong message.</summary>
        public static string Pong(string server, string server2) => $"PONG {server} {server2}";

        /// <summary>Error message.</summary>
        public static string Error(string errormessage) => $"ERROR :{errormessage}";

        /// <summary>Away message.</summary>
        public static string Away() => "AWAY";

        /// <summary>Away message.</summary>
        public static string Away(string awaytext) => $"AWAY :{awaytext}";
        
        /// <summary>Rehash message</summary>
        public static string Rehash() => "REHASH";

        /// <summary>Die message.</summary>
        public static string Die() => "DIE";

        /// <summary>Restart message.</summary>
        public static string Restart() => "RESTART";

        /// <summary>Summon message.</summary>
        public static string Summon(string user) => $"SUMMON {user}";

        /// <summary>Summon message.</summary>
        public static string Summon(string user, string target) => $"SUMMON {user} {target}" + user + " " + target;

        /// <summary>Summon message.</summary>
        public static string Summon(string user, string target, string channel) => $"SUMMON {user} {target} {channel}";

        /// <summary>Users message.</summary>
        public static string Users() => "USERS";

        /// <summary>Users message.</summary>
        public static string Users(string target) => $"USERS {target}";

        /// <summary>Wallops message.</summary>
        public static string Wallops(string wallopstext) => $"WALLOPS :{wallopstext}";

        /// <summary>Userhost message.</summary>
        public static string Userhost(string nickname) => $"USERHOST {nickname}";

        /// <summary>Userhost message.</summary>
        public static string Userhost(string[] nicknames) => $"USERHOST {string.Join(" ", nicknames)}";

        /// <summary>Ison message.</summary>
        public static string Ison(string nickname) => $"ISON {nickname}";

        /// <summary>Ison message.</summary>
        public static string Ison(string[] nicknames) => $"ISON {string.Join(" ", nicknames)}";

        /// <summary>Quit message.</summary>
        public static string Quit() => "QUIT";

        /// <summary>Quit message.</summary>
        public static string Quit(string quitmessage) => $"QUIT :{quitmessage}";

        /// <summary>Squit message.</summary>
        public static string Squit(string server, string comment) => $"SQUIT {server} :{comment}";
    }
}