﻿using System.Threading;
using System.Threading.Tasks;
using Sanakan.Common.Models.TwitchModels;

namespace Sanakan.TwitchService.Service.IrcMessageParser.Interfaces
{
    public interface IIrcMessageParser
    {
        /// <summary>
        /// Parses IRC message. Was borrowed from Twitch.Lib irc parser
        /// </summary>
        /// <param name="rawMessage">row IRC string</param>
        /// <returns>Chat message model</returns>
        IrcMessage ParseIrcMessage(string rawMessage);

        /// <summary>
        /// Parses IRC message Async. Was borrowed from Twitch.Lib irc parser
        /// </summary>
        /// <param name="rawMessage">row IRC string</param>
        /// <param name="stoppingToken">cancellation token</param>
        /// <returns>Chat message model</returns>
        Task<IrcMessage> ParseIrcMessageAsync(string rawMessage, CancellationToken stoppingToken = default);
        
    }
}