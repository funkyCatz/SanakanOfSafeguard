﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Sanakan.Common.Models.TwitchModels;
using Sanakan.TwitchService.Service.IrcMessageParser.Interfaces;

namespace Sanakan.TwitchService.Service.IrcMessageParser
{
    public class IrcMessageParser : IIrcMessageParser
    {
        public async Task<IrcMessage> ParseIrcMessageAsync(string rawMessage, CancellationToken stoppingToken = default)
        {
            return await Task.Run( () => ParseIrcMessage(rawMessage), stoppingToken);
        }
        
        public IrcMessage ParseIrcMessage(string rawMessage)
        {
            var tagDict = new Dictionary<string, string>();

            var state = ParserState.STATE_NONE;
            var starts = new[] { 0, 0, 0, 0, 0, 0 };
            var lens = new[] { 0, 0, 0, 0, 0, 0 };
            for (var i = 0; i < rawMessage.Length; ++i)
            {
                lens[(int)state] = i - starts[(int)state] - 1;
                if (state == ParserState.STATE_NONE && rawMessage[i] == '@')
                {
                    state = ParserState.STATE_V3;
                    starts[(int)state] = ++i;

                    var start = i;
                    string key = null;
                    for (; i < rawMessage.Length; ++i)
                    {
                        if (rawMessage[i] == '=')
                        {
                            key = rawMessage.Substring(start, i - start);
                            start = i + 1;
                        }
                        else if (rawMessage[i] == ';')
                        {
                            if (key == null)
                                tagDict[rawMessage.Substring(start, i - start)] = "1";
                            else
                                tagDict[key] = rawMessage.Substring(start, i - start);
                            start = i + 1;
                        }
                        else if (rawMessage[i] == ' ')
                        {
                            if (key == null)
                                tagDict[rawMessage.Substring(start, i - start)] = "1";
                            else
                                tagDict[key] = rawMessage.Substring(start, i - start);
                            break;
                        }
                    }
                }
                else if (state < ParserState.STATE_PREFIX && rawMessage[i] == ':')
                {
                    state = ParserState.STATE_PREFIX;
                    starts[(int)state] = ++i;
                }
                else if (state < ParserState.STATE_COMMAND)
                {
                    state = ParserState.STATE_COMMAND;
                    starts[(int)state] = i;
                }
                else if (state < ParserState.STATE_TRAILING && rawMessage[i] == ':')
                {
                    state = ParserState.STATE_TRAILING;
                    starts[(int)state] = ++i;
                    break;
                }
                else if (state < ParserState.STATE_TRAILING && rawMessage[i] == '+' || state < ParserState.STATE_TRAILING && rawMessage[i] == '-')
                {
                    state = ParserState.STATE_TRAILING;
                    starts[(int)state] = i;
                    break;
                }
                else if (state == ParserState.STATE_COMMAND)
                {
                    state = ParserState.STATE_PARAM;
                    starts[(int)state] = i;
                }

                while (i < rawMessage.Length && rawMessage[i] != ' ')
                    ++i;
            }

            lens[(int)state] = rawMessage.Length - starts[(int)state];
            var cmd = rawMessage.Substring(starts[(int)ParserState.STATE_COMMAND],
                lens[(int)ParserState.STATE_COMMAND]);

            var command = IrcCommandEnum.Unknown;
            switch (cmd)
            {
                case "PRIVMSG":
                    command = IrcCommandEnum.PrivMsg;
                    break;
                case "NOTICE":
                    command = IrcCommandEnum.Notice;
                    break;
                case "PING":
                    command = IrcCommandEnum.Ping;
                    break;
                case "PONG":
                    command = IrcCommandEnum.Pong;
                    break;
                case "HOSTTARGET":
                    command = IrcCommandEnum.HostTarget;
                    break;
                case "CLEARCHAT":
                    command = IrcCommandEnum.ClearChat;
                    break;
                case "USERSTATE":
                    command = IrcCommandEnum.UserState;
                    break;
                case "GLOBALUSERSTATE":
                    command = IrcCommandEnum.GlobalUserState;
                    break;
                case "NICK":
                    command = IrcCommandEnum.Nick;
                    break;
                case "JOIN":
                    command = IrcCommandEnum.Join;
                    break;
                case "PART":
                    command = IrcCommandEnum.Part;
                    break;
                case "PASS":
                    command = IrcCommandEnum.Pass;
                    break;
                case "CAP":
                    command = IrcCommandEnum.Cap;
                    break;
                case "001":
                    command = IrcCommandEnum.RPL_001;
                    break;
                case "002":
                    command = IrcCommandEnum.RPL_002;
                    break;
                case "003":
                    command = IrcCommandEnum.RPL_003;
                    break;
                case "004":
                    command = IrcCommandEnum.RPL_004;
                    break;
                case "353":
                    command = IrcCommandEnum.RPL_353;
                    break;
                case "366":
                    command = IrcCommandEnum.RPL_366;
                    break;
                case "372":
                    command = IrcCommandEnum.RPL_372;
                    break;
                case "375":
                    command = IrcCommandEnum.RPL_375;
                    break;
                case "376":
                    command = IrcCommandEnum.RPL_376;
                    break;
                case "WHISPER":
                    command = IrcCommandEnum.Whisper;
                    break;
                case "SERVERCHANGE":
                    command = IrcCommandEnum.ServerChange;
                    break;
                case "RECONNECT":
                    command = IrcCommandEnum.Reconnect;
                    break;
                case "ROOMSTATE":
                    command = IrcCommandEnum.RoomState;
                    break;
                case "USERNOTICE":
                    command = IrcCommandEnum.UserNotice;
                    break;
                case "MODE":
                    command = IrcCommandEnum.Mode;
                    break;
            }

            var parameters = rawMessage.Substring(starts[(int)ParserState.STATE_PARAM],
                lens[(int)ParserState.STATE_PARAM]);
            
            var message = rawMessage.Substring(starts[(int)ParserState.STATE_TRAILING],
                lens[(int)ParserState.STATE_TRAILING]);
            
            var hostmask = rawMessage.Substring(starts[(int)ParserState.STATE_PREFIX],
                lens[(int)ParserState.STATE_PREFIX]);
            return new IrcMessage(command, new[] { parameters, message }, hostmask, tagDict);
        }

        private enum ParserState
        {
            STATE_NONE,
            STATE_V3,
            STATE_PREFIX,
            STATE_COMMAND,
            STATE_PARAM,
            STATE_TRAILING
        };
    }
}