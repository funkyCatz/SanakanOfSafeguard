﻿using System.Threading;
using System.Threading.Tasks;

namespace Sanakan.TwitchService.Service.Interfaces
{
    public interface ITwitchChatService
    {
        /// <summary>
        /// Handles twitch raw message
        /// </summary>
        /// <param name="rawMessage">twitch raw message</param>
        void HandleMessage(string rawMessage);

        /// <summary>
        /// Handles twitch raw message
        /// </summary>
        /// <param name="rawMessage">twitch raw message</param>
        /// <param name="stoppingToken">cancellation token</param>
        /// <returns>none</returns>
        Task HandleMessageAsync(string rawMessage, CancellationToken stoppingToken);

        /// <summary>
        /// Implements login process for twitch irc service
        /// </summary>
        /// <param name="stoppingToken">CancellationToken</param>
        /// <returns></returns>
        Task StartAsync(CancellationToken stoppingToken);
        
        /// <summary>
        /// Stops service
        /// </summary>
        /// <param name="stoppingToken">cancellation token</param>
        /// <returns></returns>
        Task StopAsync(CancellationToken stoppingToken);

        /// <summary>
        /// Joins twitch channel
        /// </summary>
        /// <param name="channelName">channel name</param>
        /// <param name="stoppingToken">cancellation token</param>
        /// <returns></returns>
        Task JoinChannelAsync(string channelName, CancellationToken stoppingToken);


        /// <summary>
        /// Leaves twitch channel
        /// </summary>
        /// <param name="channelName">channel name</param>
        /// <param name="stoppingToken">cancellation token</param>
        /// <returns></returns>
        Task LeaveChannelAsync(string channelName, CancellationToken stoppingToken);

    }
}