﻿using System.Threading.Tasks;
using Sanakan.Common.Models.TwitchModels;

namespace Sanakan.TwitchService.Service.Interfaces
{
    public interface IIrcMessage
    {
        IrcCommandEnum Command { get; }
        
        void Handle(IrcMessage ircMessage);
        
        Task HandleAsync(IrcMessage ircMessage);
    }
}