﻿using System.Threading.Tasks;
using Sanakan.Common.Models.TwitchModels;

namespace Sanakan.TwitchService.Service.Interfaces
{
    public interface IIrcCommandMessageHandler
    {
        void Handle(IrcMessage ircMessage);
        
        Task HandleAsync(IrcMessage ircMessage);
    }
}