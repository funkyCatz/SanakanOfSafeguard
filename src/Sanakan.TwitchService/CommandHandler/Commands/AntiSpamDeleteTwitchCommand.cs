﻿using System;
using System.Linq;
using System.Threading.Tasks;
using MediatR;
using Sanakan.Common.Models.TwitchModels;
using Sanakan.DataAccess.Commands;
using Sanakan.DataAccess.Domain.Entities;
using Sanakan.DataAccess.Queries;
using Sanakan.TwitchService.CommandHandler.Commands.Interfaces;

namespace Sanakan.TwitchService.CommandHandler.Commands
{
    public class AntiSpamDeleteTwitchCommand: ITwitchCommand
    {
        private readonly IMediator _mediator;
        
        public string Name { get; }

        public AntiSpamDeleteTwitchCommand(IMediator mediator)
        {
            _mediator = mediator;

            Name = "spam-delete";
        }
        public async Task ExecuteAsync(ChatCommand command)
        {
            if (CheckPermissions(command))
            {
                await _mediator.Send(new DeleteFiltrationMessageCommand()
                {
                    FiltrationMessage = new FiltrationMessage
                    {
                        Author = command.ChatMessage.UserName,
                        Text = command.ArgumentsAsString,
                        TimeStamp = DateTime.UtcNow,
                        Channel = command.ChatMessage.Channel,
                        UserId = int.Parse(command.ChatMessage.UserId)
                    }
                });
            }
        }

        public void Execute(ChatCommand command)
        {
            var task = Task.Run(async () => await ExecuteAsync(command));
            task.Wait();
        }
        
        public bool CheckPermissions(ChatCommand command) => command.ChatMessage.IsBroadcaster;
    }
}