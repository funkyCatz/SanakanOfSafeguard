﻿using System.Threading.Tasks;
using Sanakan.Common.Models.TwitchModels;

namespace Sanakan.TwitchService.CommandHandler.Commands.Interfaces
{
    public interface ITwitchCommand
    {
        public string Name { get; }

        public Task ExecuteAsync(ChatCommand command);
        
        public void Execute(ChatCommand command);
        

        public bool CheckPermissions(ChatCommand command) => command.ChatMessage.IsBroadcaster;
    }
}