﻿using System;
using System.Linq;
using System.Threading.Tasks;
using MediatR;
using Sanakan.Common.Models.TwitchModels;
using Sanakan.DataAccess.Commands;
using Sanakan.DataAccess.Domain.Entities;
using Sanakan.DataAccess.Queries;
using Sanakan.DataAccess.Repository.Interfaces;
using Sanakan.TwitchService.CommandHandler.Commands.Interfaces;

namespace Sanakan.TwitchService.CommandHandler.Commands
{
    public class AntiSpamAddTwitchCommand : ITwitchCommand
    {
        private readonly IMediator _mediator;
        
        public string Name { get; }

        public AntiSpamAddTwitchCommand(IMediator mediator)
        {
            _mediator = mediator;

            Name = "spam-add";
        }
        
        public async Task ExecuteAsync(ChatCommand command)
        {
            if (CheckPermissions(command))
            {
                var filtrationEntities = await _mediator.Send(new GetFiltersQuery
                {
                    ChannelName = command.ChatMessage.Channel
                });
                
                if (!filtrationEntities.Any(x =>
                    string.Equals(x.Text, command.ArgumentsAsString, StringComparison.OrdinalIgnoreCase)))
                {
                    await _mediator.Send(new AddFiltrationMessageCommand
                    {
                        FiltrationMessage = new FiltrationMessage
                        {
                            Author = command.ChatMessage.UserName,
                            Text = command.ArgumentsAsString,
                            TimeStamp = DateTime.UtcNow,
                            Channel = command.ChatMessage.Channel,
                            UserId = int.Parse(command.ChatMessage.UserId)
                        }
                    });
                }
            }
        }

        public bool CheckPermissions(ChatCommand command) => command.ChatMessage.IsBroadcaster;

        public void Execute(ChatCommand command)
        {
            var task = Task.Run(() => ExecuteAsync(command));
            task.Wait();
        }
    }
}