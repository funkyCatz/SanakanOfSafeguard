﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Sanakan.Common.Models.TwitchModels;
using Sanakan.TwitchService.CommandHandler.Commands.Interfaces;
using Sanakan.TwitchService.CommandHandler.Interfaces;
using Sanakan.TwitchService.Options;

namespace Sanakan.TwitchService.CommandHandler
{
    public class TwitchCommandsHandler : ITwitchCommandsHandler
    {
        private IDictionary<string, ITwitchCommand> TwitchCommandsLookup { get; }
        private readonly ILogger<TwitchCommandsHandler> _logger;
        private readonly char[] _commandTokens;

        public TwitchCommandsHandler(ILogger<TwitchCommandsHandler> logger,
            IEnumerable<ITwitchCommand> commands,
            IOptions<CommandOptions> commandOptions)
        {
            TwitchCommandsLookup = commands.ToDictionary( key => key.Name);
            _commandTokens = commandOptions.Value.CommandTokens.Split(' ', StringSplitOptions.RemoveEmptyEntries)
                .Select(x => x.First()).ToArray();
            _logger = logger;
        }
        
        public async Task ExecuteAsync(ChatMessage message, CancellationToken stoppingToken = default)
        {
            if (IsCommand(message))
            {
                ChatCommand command = new ChatCommand(message);

                if (!TwitchCommandsLookup.TryGetValue(command.CommandName, out var commandHandler))
                {
                    _logger.LogInformation($"Command {command.CommandName} does not have a command handler.");
                }

                await commandHandler.ExecuteAsync(command);
            }
        }

        public void Execute(ChatMessage message)
        {
            var task = Task.Run(async () => await ExecuteAsync(message));
            task.Wait();
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private bool IsCommand(ChatMessage message)
        {
            if (_commandTokens.Any())
            {
                if (_commandTokens.Contains(message.Message.First()))
                {
                    return true;
                }
            }

            return false;
        }
    }
}