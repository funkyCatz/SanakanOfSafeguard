﻿using System.Threading;
using System.Threading.Tasks;
using Sanakan.Common.Models.TwitchModels;

namespace Sanakan.TwitchService.CommandHandler.Interfaces
{
    public interface ITwitchCommandsHandler
    {
        Task ExecuteAsync(ChatMessage message, CancellationToken stoppingToken = default);
        void Execute(ChatMessage message);
    }
}