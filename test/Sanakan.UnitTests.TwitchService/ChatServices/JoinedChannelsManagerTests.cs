﻿using System.Linq;
using FluentAssertions;
using NUnit.Framework;
using Sanakan.Common.Models.TwitchModels;
using Sanakan.TwitchService.Service.JoinedChannelsManager;
using Sanakan.TwitchService.TwitchService.JoinedChannelsManager.Interfaces;

namespace Sanakan.UnitTests.TwitchService.ChatServices
{
    [TestFixture(TestName = "JoinedChannelsManagerTests")]
    public class JoinedChannelsManagerTests
    {
        private readonly IJoinedChannelsManager _joinedChannelsManager;

        public JoinedChannelsManagerTests()
        {
            _joinedChannelsManager = new JoinedChannelsManager();
        }

        [SetUp]
        public void Setup()
        {
            _joinedChannelsManager.AddChannel(new JoinedChannel("test_channel_1"));
            _joinedChannelsManager.AddChannel(new JoinedChannel("test_channel_2"));
            _joinedChannelsManager.AddChannel(new JoinedChannel("test_channel_3"));
        }

        [Test]
        public void AddChannelTest()
        {
            _joinedChannelsManager.AddChannel(new JoinedChannel("test_channel_4"));

            _joinedChannelsManager.Channels.Should().NotBeNull();
            _joinedChannelsManager.Channels.Count().Should().Be(4);

            var channel = _joinedChannelsManager.GetChannel("test_channel_4");

            channel.Should().NotBeNull();
        }

        [Test]
        public void ResetTest()
        {
            _joinedChannelsManager.Reset();

            _joinedChannelsManager.Channels.Should().NotBeNull();
            _joinedChannelsManager.Channels.Count.Should().Be(0);
        }

        [Test]
        public void RemoveChannelTest()
        {
            _joinedChannelsManager.RemoveChannel("test_channel_3");
            
            _joinedChannelsManager.Channels.Should().NotBeNull();
            _joinedChannelsManager.Channels.TryGetValue("test_channel_3", out _).Should().Be(false);
        }
    }
}