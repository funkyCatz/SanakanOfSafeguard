﻿using FluentAssertions;
using NUnit.Framework;
using Sanakan.TwitchService.Service.IrcMessageParser;
using Sanakan.TwitchService.Service.IrcMessageParser.Interfaces;

namespace Sanakan.UnitTests.TwitchService.ChatServices
{
    [TestFixture( TestName = "IIrcMessageParserTests")]
    public class IIrcMessageParserTests
    {
        private const string _rawTwitchIrcMessage =
            @"@badge-info=;badges=vip/1,glhf-pledge/1;color=#FF69B4;display-name=the_best_russian_bear;emotes=;flags=;id=bd70aaab-6642-4e10-a4b0-2deec8c5bbbc;mod=0;room-id=254532867;subscriber=0;tmi-se
nt-ts=1588622438654;turbo=0;user-id=182274590;user-type= :the_best_russian_bear!the_best_russian_bear@the_best_russian_bear.tmi.twitch.tv PRIVMSG #exschnitzel :Графон 2009 года";

        private readonly IIrcMessageParser _ircMessageParser;

        public IIrcMessageParserTests()
        {
            _ircMessageParser = new IrcMessageParser();
        }
        
        [Test]
        public void ParserBasicTest()
        {
            var parsedMessage = _ircMessageParser.ParseIrcMessage(_rawTwitchIrcMessage);
            
            parsedMessage.Should().NotBeNull();
            parsedMessage.Channel.Should().Match(x => string.Equals(x, "exschnitzel"));
            parsedMessage.User.Should().Match(x => string.Equals(x, "the_best_russian_bear"));
            parsedMessage.Message.Should().Match(x => string.Equals(x, "Графон 2009 года"));
        }
    }
}