﻿using Raven.Client.Documents;
using Raven.TestDriver;
using Sanakan.DataAccess.RavenDB;

namespace Sanakan.UnitTests.DataAccess.Mocks
{
    public class DocumentStoreLifecycleMock : RavenTestDriver, IDocumentStoreLifecycle
    {
        public IDocumentStore Store { get; }

        public DocumentStoreLifecycleMock()
        {
            Store = GetDocumentStore();
        }
    }
}