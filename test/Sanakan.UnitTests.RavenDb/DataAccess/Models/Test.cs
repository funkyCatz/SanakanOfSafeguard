﻿using System;
using Sanakan.DataAccess.Domain.Entities;

namespace Sanakan.UnitTests.DataAccess.Models
{
    public class Test : Identity
    {
        public string Name { get; set; }
        public DateTime TimeStamp { get; set; }
        public int RndNumber { get; set; }
    }
}