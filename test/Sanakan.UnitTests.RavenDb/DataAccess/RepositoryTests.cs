﻿using System;
using System.Linq;
using System.Threading.Tasks;
using FluentAssertions;
using NUnit.Framework;
using Sanakan.DataAccess.RavenDB.Repository;
using Sanakan.UnitTests.DataAccess.Mocks;
using Sanakan.UnitTests.DataAccess.Models;

namespace Sanakan.UnitTests.DataAccess
{
    [TestFixture(TestName = "Repository test using RavenDB Test Driver"), Ignore("Obsolete")]
    public class RepositoryTests
    {
        private RevenDbRepository<Test> _revenDbRepository;

        public RepositoryTests()
        {
            _revenDbRepository = new RevenDbRepository<Test>(
                new  DocumentStoreLifecycleMock());
        }
        
        [Test(Description = "This test creates TestModel entity and tries to save it, then tries to get all entities from DB")]
        public async Task CreateGetTest()
        {
            Test test = new Test
            {
                Name = "test",
                RndNumber = 10,
                TimeStamp = DateTime.UtcNow
            };

            await _revenDbRepository.UpdateAsync(test);

            var entity = await _revenDbRepository.GetAllAsync();
            
            entity.Should().NotBeNull();
            entity.Count().Should().Be(1);
            entity.FirstOrDefault().Name.Should().Be("test");
            entity.FirstOrDefault().RndNumber.Should().Be(10);
        }

        [Test(Description = "Find entity testing")]
        public async Task FindEntityTest()
        {
            Test testModel1 = new Test
            {
                Name = "test",
                RndNumber = 10,
                TimeStamp = DateTime.UtcNow
            };
            
            Test testModel2 = new Test
            {
                Name = "test2",
                RndNumber = 11,
                TimeStamp = DateTime.UtcNow
            };

            await _revenDbRepository.UpdateAsync(testModel1);
            await _revenDbRepository.UpdateAsync(testModel2);

            var entities = await _revenDbRepository.FindAsync(x => x.RndNumber == 10);

            entities.Should().NotBeNull();
            var entity = entities.FirstOrDefault();

            entity.Name.Should().Be("test");
            entity.RndNumber.Should().Be(10);
        }
    }
}