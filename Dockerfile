FROM mcr.microsoft.com/dotnet/core/sdk:3.1-focal AS base
WORKDIR /app

FROM mcr.microsoft.com/dotnet/core/sdk:3.1-buster AS build
WORKDIR /src

# Copy csproj and restore as distinct layers
COPY *.sln ./
COPY ./src ./src
COPY ./test ./test

RUN dotnet restore
WORKDIR "/src/."
RUN dotnet build -c Release -o /app/build

FROM build AS publish
RUN dotnet publish -c Release -o /app/publish

FROM base AS final
WORKDIR /app

COPY --from=publish /app/publish .
# Copy certificates for Raven or Mongo
COPY ./certificates .

ENTRYPOINT ["dotnet", "Sanakan.ChatService.dll"]